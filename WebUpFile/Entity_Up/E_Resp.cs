﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity_Up
{
    public class E_Resp
    {
        public string Message { get; set; }
        public bool Status { get; set; }
        public string errorMessage { get; set; }
        public object Obj { get; set; }
    }
}
