﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity_Up
{
    public class E_XmlFields
    {
        public string c_server { get; set; }
        public string c_server2 { get; set; }
        public string c_database { get; set; }
        public string c_database2 { get; set; }
        public string c_master { get; set; }
        public string c_user { get; set; }
        public string c_user2 { get; set; }
        public string c_password { get; set; }
        public string c_password2 { get; set; }
        public string p_transport { get; set; }
        public string p_warehouse { get; set; }
        public string p_priceType { get; set; }
        public string p_ivatype { get; set; }
        public string p_delimiter { get; set; }
        public string p_user { get; set; }
        public string p_money { get; set; }
        public string p_principal { get; set; }
        public string p_customer { get; set; }
        public string p_sucursal { get; set; }
        public string p_artp { get; set; }
        public string p_arts { get; set; }
        public bool ok { get; set; }
        public string message { get; set; }
    }
}
