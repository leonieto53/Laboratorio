﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity_Up
{
    public class E_CotizacionHead
    {
        public string sDoc_Num { get; set; } //CHAR(20)
        public string sDescrip { get; set; } //VARCHAR(60)
        public string sCo_Cli { get; set; } // CHAR(16)
        public string sCo_Cta_Ingr_Egr { get; set; }   // CHAR(20) = NULL 
        public string sCo_Tran { get; set; }   // CHAR(6)
        public string sCo_Mone { get; set; }   // CHAR(6) 
        public string sCo_Ven { get; set; }  // CHAR(6)
        public string sCo_Cond { get; set; }    // CHAR(6)
        public DateTime sdFec_Emis { get; set; }   // SMALLDATETIME
        public DateTime sdFec_Venc { get; set; }   //SMALLDATETIME 
        public DateTime sdFec_Reg { get; set; }  // SMALLDATETIME
        public bool bAnulado { get; set; }  // BIT 
        public string sStatus { get; set; }  // CHAR(1)
        public Decimal deTasa { get; set; }  // DECIMAL(18, 5)
        public string sN_Control { get; set; }  // VARCHAR(20)
        public string sNro_Doc { get; set; }  // VARCHAR(20) = NULL
        public string sPorc_Desc_Glob { get; set; }  // VARCHAR(15) = NULL
        public Decimal deMonto_Desc_Glob { get; set; }  // DECIMAL(18, 2)
        public string sPorc_Reca { get; set; }  // VARCHAR(15) = NULL 
        public Decimal deMonto_Reca { get; set; }  //  DECIMAL(18, 2)
        public Decimal deSaldo { get; set; }  // DECIMAL(18, 2) 
        public Decimal deTotal_Bruto { get; set; }  // DECIMAL(18, 2) 
        public Decimal deMonto_Imp { get; set; }  // DECIMAL(18, 2)
        public Decimal deMonto_Imp2 { get; set; }  // DECIMAL(18, 2) 
        public Decimal deMonto_Imp3 { get; set; }  // DECIMAL(18, 2) 
        public Decimal deOtros1 { get; set; }  // DECIMAL(18, 2) 
        public Decimal deOtros2 { get; set; }  // DECIMAL(18, 2) 
        public Decimal deOtros3 { get; set; }  // DECIMAL(18, 2) 
        public Decimal deTotal_Neto { get; set; }  // DECIMAL(18, 2) = NULL 
        public string sDis_Cen { get; set; }  // VARCHAR(MAX) 
        public string sComentario { get; set; }  // VARCHAR(MAX) 
        public string sDir_Ent { get; set; }  // VARCHAR(MAX) 
        public bool bContrib { get; set; }  // BIT
        public bool bImpresa { get; set; }  // BIT 
        public string sSalestax { get; set; }  // CHAR(8) 
        public string sImpfis { get; set; }  // VARCHAR(20) 
        public string sImpfisfac { get; set; }  // VARCHAR(20) 
        public bool bVen_Ter { get; set; }  // BIT
        public string sCampo1 { get; set; }  // VARCHAR(60) = NULL 
        public string sCampo2 { get; set; }  // VARCHAR(60) = NULL 
        public string sCampo3 { get; set; }  // VARCHAR(60) = NULL 
        public string sCampo4 { get; set; }  // VARCHAR(60) = NULL 
        public string sCampo5 { get; set; }  // VARCHAR(60) = NULL 
        public string sCampo6 { get; set; }  // VARCHAR(60) = NULL 
        public string sCampo7 { get; set; }  // VARCHAR(60) = NULL 
        public string sCampo8 { get; set; }  // VARCHAR(60) = NULL 
        public string sCo_Us_In { get; set; }  // CHAR(6)
        public string sCo_Sucu_In { get; set; }  // CHAR(6) 
        public string sRevisado { get; set; }  // CHAR(1) = NULL 
        public string sTrasnfe { get; set; }  // CHAR(1) = NULL 
        public string sMaquina { get; set; }  // VARCHAR(60) = NULL
    }
}
