﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity_Up
{
    public class E_CotizacionDetail
    {
        public Int32 iReng_Num { get; set; } // INT
        public string sDoc_Num { get; set; }  // CHAR(20) 
        public string sCo_Art { get; set; }  // CHAR(30) 
        public string sDes_Art { get; set; }  // VARCHAR(120) = NULL 
        public string sCo_Uni { get; set; }  // CHAR(6) 
        public string sSco_Uni { get; set; }  // CHAR(6) = NULL 
        public string sCo_Alma { get; set; } // CHAR(6) 
        public string sCo_Precio { get; set; }  // CHAR(6) = NULL 
        public string sTipo_Imp { get; set; }  // CHAR(1) 
        public string sTipo_Imp2 { get; set; }  // CHAR(1) = NULL 
        public string sTipo_Imp3 { get; set; }  // CHAR(1) = NULL 
        public Decimal deTotal_Art { get; set; }  // DECIMAL(18 5) 
        public Decimal deSTotal_Art { get; set; }  // DECIMAL(18 5) 
        public Decimal dePrec_Vta { get; set; }  //  DECIMAL(18 5) 
        public string sPorc_Desc { get; set; }  // VARCHAR(15) = NULL 
        public Decimal deMonto_Desc { get; set; }  // DECIMAL(18 5) = NULL 
        public Decimal deReng_Neto { get; set; }  // DECIMAL(18 2) 
        public Decimal dePendiente { get; set; }   // DECIMAL(18 5) 
        public Decimal dePendiente2 { get; set; }  // DECIMAL(18 5) 
        public Decimal deMonto_Desc_Glob { get; set; }   //  DECIMAL(18 5) 
        public Decimal deMonto_reca_Glob { get; set; }  // DECIMAL(18 5) 
        public Decimal deOtros1_glob { get; set; }  // DECIMAL(18 5) 
        public Decimal deOtros2_glob { get; set; }   // DECIMAL(18 5) 
        public Decimal deOtros3_glob { get; set; }  // DECIMAL(18 5) 
        public Decimal deMonto_imp_afec_glob { get; set; }  // DECIMAL(18 5) 
        public Decimal deMonto_imp2_afec_glob { get; set; }  // DECIMAL(18 5) 
        public Decimal deMonto_imp3_afec_glob { get; set; }  // DECIMAL(18 5) 
        public string sTipo_Doc { get; set; }  // CHAR(4) 
        public Guid gRowguid_Doc { get; set; }  // UNIQUEIDENTIFIER
        public string sNum_Doc { get; set; }  // VARCHAR(20) 
        public Decimal dePorc_Imp { get; set; }  // DECIMAL(18 5) 
        public Decimal dePorc_Imp2 { get; set; }  // DECIMAL(18 5) 
        public Decimal dePorc_Imp3 { get; set; }  // DECIMAL(18 5) 
        public Decimal deMonto_Imp { get; set; }  // DECIMAL(18 5) 
        public Decimal deMonto_Imp2 { get; set; }  // DECIMAL(18 5) 
        public Decimal deMonto_Imp3 { get; set; }  // DECIMAL(18 5) 
        public Decimal deOtros { get; set; }  // DECIMAL(18 5) = NULL 
        public Decimal deTotal_Dev { get; set; }  // DECIMAL(18 5) 
        public Decimal deMonto_Dev { get; set; }  // DECIMAL(18 5) = NULL 
        public string sComentario { get; set; }  // VARCHAR(MAX) 
        public string sDis_Cen { get; set; }  // VARCHAR(MAX) = NULL 
        public string sCo_Sucu_In { get; set; }  // CHAR(6) 
        public string sCo_Us_In { get; set; }  // CHAR(6) 
        public string sREVISADO { get; set; }  // CHAR(1) 
        public string sTRASNFE { get; set; }  // CHAR(1) 
        public string sMaquina { get; set; }  // VARCHAR(60) = NULL
    }
}
