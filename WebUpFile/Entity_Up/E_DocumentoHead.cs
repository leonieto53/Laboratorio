﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity_Up
{
    public class E_DocumentoHead
    {
        public string sCo_Tipo_Doc { get; set; } //CHAR(6)
        public string sNro_Doc { get; set; } //CHAR(20)
        public string sCo_Cli { get; set; } // CHAR(16)
        public string sCo_Ven { get; set; }  // CHAR(6)
        public string sCo_Mone { get; set; }   // CHAR(6) 
        public string sMov_Ban { get; set; }   // CHAR(20) 
        public string sCo_Cta_Ingr_Egr { get; set; }   // CHAR(20) = NULL 
        public Decimal deTasa { get; set; }  // DECIMAL(18, 5)
        public string sDescrip { get; set; } //VARCHAR(60)
        public string sObserva { get; set; } //VARCHAR(MAX)
        public DateTime sdFec_Emis { get; set; }   // SMALLDATETIME
        public DateTime sdFec_Venc { get; set; }   //SMALLDATETIME 
        public DateTime sdFec_Reg { get; set; }  // SMALLDATETIME
        public bool bAnulado { get; set; }  // BIT
        public bool bAut { get; set; }  // BIT
        public bool bContrib { get; set; }  // BIT
        public string sDoc_Orig { get; set; } //VARCHAR(6)
        public string sNro_Orig { get; set; } //VARCHAR(20)
        public string sNro_Che { get; set; } //VARCHAR(20)
        public Decimal deMonto_Imp { get; set; }  // DECIMAL(18, 2)
        public Decimal deSaldo { get; set; }  // DECIMAL(18, 2) 
        public Decimal deTotal_Bruto { get; set; }  // DECIMAL(18, 2) 
        public Decimal deMonto_Desc_Glob { get; set; }  // DECIMAL(18, 2)
        public string sPorc_Desc_Glob { get; set; }  // VARCHAR(15) = NULL
        public string sPorc_Reca { get; set; }  // VARCHAR(15) = NULL 
        public Decimal deMonto_Reca { get; set; }  //  DECIMAL(18, 2)
        public Decimal deTotal_Neto { get; set; }  // DECIMAL(18, 2) = NULL 
        public Decimal deMonto_Imp2 { get; set; }  // DECIMAL(18, 2) 
        public Decimal deMonto_Imp3 { get; set; }  // DECIMAL(18, 2) 
        public string sTipo_Imp { get; set; }   // CHAR(1) 
        public Int32 iTipo_Origen { get; set; }   // INT(1) 
        public Decimal dePorc_Imp { get; set; }  // DECIMAL(18, 2)
        public Decimal dePorc_Imp2 { get; set; }  // DECIMAL(18, 2)
        public Decimal dePorc_Imp3 { get; set; }  // DECIMAL(18, 2)
        public string sNum_Comprobante { get; set; }   // CHAR(14) 
        public string sN_Control { get; set; }  // VARCHAR(20)
        public string sDis_Cen { get; set; }  // VARCHAR(MAX) 
        public Decimal deComis1 { get; set; }  // DECIMAL(18, 2)
        public Decimal deComis2 { get; set; }  // DECIMAL(18, 2)
        public Decimal deComis3 { get; set; }  // DECIMAL(18, 2)
        public Decimal deComis4 { get; set; }  // DECIMAL(18, 2)
        public Decimal deComis5 { get; set; }  // DECIMAL(18, 2)
        public Decimal deComis6 { get; set; }  // DECIMAL(18, 2)
        public Decimal deAdicional { get; set; }  // DECIMAL(18, 2)
        public string sSalestax { get; set; }  // CHAR(8) 
        public bool bVen_Ter { get; set; }  // BIT
        public string sImpfis { get; set; }  // VARCHAR(20) 
        public string sImpfisfac { get; set; }  // VARCHAR(20) 
        public string sImp_nro_z { get; set; }  // CHAR(15) 
        public Decimal deOtros1 { get; set; }  // DECIMAL(18, 2) 
        public Decimal deOtros2 { get; set; }  // DECIMAL(18, 2) 
        public Decimal deOtros3 { get; set; }  // DECIMAL(18, 2) 
        public string sCampo1 { get; set; }  // VARCHAR(60) = NULL 
        public string sCampo2 { get; set; }  // VARCHAR(60) = NULL 
        public string sCampo3 { get; set; }  // VARCHAR(60) = NULL 
        public string sCampo4 { get; set; }  // VARCHAR(60) = NULL 
        public string sCampo5 { get; set; }  // VARCHAR(60) = NULL 
        public string sCampo6 { get; set; }  // VARCHAR(60) = NULL 
        public string sCampo7 { get; set; }  // VARCHAR(60) = NULL 
        public string sCampo8 { get; set; }  // VARCHAR(60) = NULL 
        public string sRevisado { get; set; }  // CHAR(1) = NULL 
        public string sTrasnfe { get; set; }  // CHAR(1) = NULL 
        public string sCo_Sucu_In { get; set; }  // CHAR(6) 
        public string sCo_Us_In { get; set; }  // CHAR(6)
        public string sMaquina { get; set; }  // VARCHAR(60) = NULL
    }
}
