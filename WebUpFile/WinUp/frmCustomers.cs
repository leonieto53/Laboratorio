﻿using Business_Up;
using Entity_Up;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;


namespace WinUp
{
    public partial class frmCustomer : Form
    {

        private List<E_Customer> cli = null;

        public frmCustomer()
        {
            InitializeComponent();
        }

        public frmCustomer(List<E_Customer> _cli) : this()
        {
            cli = _cli;
        }

        private int posY = 0;
        private int posX = 0;

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            MovePanel(e);
        }

        private void frmSetup_Load(object sender, EventArgs e)
        {
            dgvData.DataSource = cli;
            dgvData.Columns[0].Width = 120; // RIF
            dgvData.Columns[1].Width = 417; // Nombre Cliente
        }

        private void MovePanel(MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                posX = e.X;
                posY = e.Y;
            }
            else
            {
                Left = Left + (e.X - posX);
                Top = Top + (e.Y - posY);
            }
        }

        private void butExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void frmConection_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }


    }
}
