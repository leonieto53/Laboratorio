﻿using Business_Up;
using Entity_Up;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;


namespace WinUp
{
    public partial class frmAlgoritmo : Form
    {

        private List<E_CotizacionHead> heads = null;

        public frmAlgoritmo()
        {
            InitializeComponent();
        }

        public frmAlgoritmo(List<E_CotizacionHead> _heads) : this()
        {
            heads = _heads;
        }

        private int posY = 0;
        private int posX = 0;

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            MovePanel(e);
        }

        private void frmSetup_Load(object sender, EventArgs e)
        {
            dgvData.DataSource = heads;

            foreach (DataGridViewColumn col in dgvData.Columns)
            {
                if (col.Index != 8 && col.Index != 21 && col.Index != 22 && col.Index != 28 && col.Index != 38
                    && col.Index != 39 && col.Index != 40)
                {
                    //col.Visible = false;
                }
            }
                //dgvData.Columns[0].Width = 120; // RIF
                //dgvData.Columns[1].Width = 417; // Nombre Cliente
        }

        private void MovePanel(MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                posX = e.X;
                posY = e.Y;
            }
            else
            {
                Left = Left + (e.X - posX);
                Top = Top + (e.Y - posY);
            }
        }

        private void butExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void frmConection_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }


    }
}
