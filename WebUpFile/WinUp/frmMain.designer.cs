﻿namespace WinUp
{
    partial class frmMain
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.panelTop = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.butRestore = new System.Windows.Forms.Button();
            this.butMinimize = new System.Windows.Forms.Button();
            this.butMaximize = new System.Windows.Forms.Button();
            this.butExit = new System.Windows.Forms.Button();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.butSeries = new System.Windows.Forms.Button();
            this.butAbout = new System.Windows.Forms.Button();
            this.butParameters = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.butSetup = new System.Windows.Forms.Button();
            this.butUpload = new System.Windows.Forms.Button();
            this.picLine = new System.Windows.Forms.PictureBox();
            this.labelMenu2 = new System.Windows.Forms.Label();
            this.labelMenu1 = new System.Windows.Forms.Label();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.panelDesktop = new System.Windows.Forms.Panel();
            this.panelFoot = new System.Windows.Forms.Panel();
            this.butFoot4 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labdb = new System.Windows.Forms.Label();
            this.butFoot3 = new System.Windows.Forms.Button();
            this.butFoot2 = new System.Windows.Forms.Button();
            this.butFoot1 = new System.Windows.Forms.Button();
            this.butFoot5 = new System.Windows.Forms.Button();
            this.panelTop.SuspendLayout();
            this.panelMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.panelDesktop.SuspendLayout();
            this.panelFoot.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.panelTop.Controls.Add(this.button1);
            this.panelTop.Controls.Add(this.butRestore);
            this.panelTop.Controls.Add(this.butMinimize);
            this.panelTop.Controls.Add(this.butMaximize);
            this.panelTop.Controls.Add(this.butExit);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1100, 42);
            this.panelTop.TabIndex = 0;
            this.panelTop.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelTop_MouseMove);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = global::WinUp.Properties.Resources.menu24Negro;
            this.button1.Location = new System.Drawing.Point(14, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(40, 33);
            this.button1.TabIndex = 1;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // butRestore
            // 
            this.butRestore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butRestore.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.butRestore.FlatAppearance.BorderSize = 0;
            this.butRestore.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(171)))), ((int)(((byte)(171)))));
            this.butRestore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butRestore.Image = global::WinUp.Properties.Resources.restaurar16Negro;
            this.butRestore.Location = new System.Drawing.Point(1019, 10);
            this.butRestore.Name = "butRestore";
            this.butRestore.Size = new System.Drawing.Size(30, 23);
            this.butRestore.TabIndex = 6;
            this.butRestore.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butRestore.UseVisualStyleBackColor = false;
            this.butRestore.Visible = false;
            this.butRestore.Click += new System.EventHandler(this.butRestore_Click);
            // 
            // butMinimize
            // 
            this.butMinimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butMinimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.butMinimize.FlatAppearance.BorderSize = 0;
            this.butMinimize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(171)))), ((int)(((byte)(171)))));
            this.butMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butMinimize.Image = global::WinUp.Properties.Resources.guionNegro;
            this.butMinimize.Location = new System.Drawing.Point(979, 10);
            this.butMinimize.Name = "butMinimize";
            this.butMinimize.Size = new System.Drawing.Size(30, 23);
            this.butMinimize.TabIndex = 7;
            this.butMinimize.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butMinimize.UseVisualStyleBackColor = false;
            this.butMinimize.Click += new System.EventHandler(this.butMinimize_Click);
            // 
            // butMaximize
            // 
            this.butMaximize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butMaximize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.butMaximize.FlatAppearance.BorderSize = 0;
            this.butMaximize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(171)))), ((int)(((byte)(171)))));
            this.butMaximize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butMaximize.Image = global::WinUp.Properties.Resources.CuadradoNegro16;
            this.butMaximize.Location = new System.Drawing.Point(1019, 10);
            this.butMaximize.Name = "butMaximize";
            this.butMaximize.Size = new System.Drawing.Size(30, 23);
            this.butMaximize.TabIndex = 3;
            this.butMaximize.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butMaximize.UseVisualStyleBackColor = false;
            this.butMaximize.Click += new System.EventHandler(this.butMaximize_Click);
            // 
            // butExit
            // 
            this.butExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.butExit.FlatAppearance.BorderSize = 0;
            this.butExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.butExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butExit.Image = global::WinUp.Properties.Resources.x_salida_Negro16;
            this.butExit.Location = new System.Drawing.Point(1059, 10);
            this.butExit.Name = "butExit";
            this.butExit.Size = new System.Drawing.Size(30, 23);
            this.butExit.TabIndex = 5;
            this.butExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butExit.UseVisualStyleBackColor = false;
            this.butExit.Click += new System.EventHandler(this.butExit_Click);
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(234)))), ((int)(((byte)(239)))));
            this.panelMenu.Controls.Add(this.butSeries);
            this.panelMenu.Controls.Add(this.butAbout);
            this.panelMenu.Controls.Add(this.butParameters);
            this.panelMenu.Controls.Add(this.button3);
            this.panelMenu.Controls.Add(this.butSetup);
            this.panelMenu.Controls.Add(this.butUpload);
            this.panelMenu.Controls.Add(this.picLine);
            this.panelMenu.Controls.Add(this.labelMenu2);
            this.panelMenu.Controls.Add(this.labelMenu1);
            this.panelMenu.Controls.Add(this.picLogo);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenu.Location = new System.Drawing.Point(0, 42);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(215, 658);
            this.panelMenu.TabIndex = 1;
            // 
            // butSeries
            // 
            this.butSeries.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butSeries.FlatAppearance.BorderSize = 0;
            this.butSeries.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.butSeries.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butSeries.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butSeries.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butSeries.Image = global::WinUp.Properties.Resources._grid3;
            this.butSeries.Location = new System.Drawing.Point(0, 179);
            this.butSeries.Name = "butSeries";
            this.butSeries.Padding = new System.Windows.Forms.Padding(0, 0, 50, 0);
            this.butSeries.Size = new System.Drawing.Size(215, 42);
            this.butSeries.TabIndex = 11;
            this.butSeries.Text = "       Series";
            this.butSeries.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butSeries.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butSeries.UseVisualStyleBackColor = true;
            this.butSeries.Click += new System.EventHandler(this.butSeries_Click);
            // 
            // butAbout
            // 
            this.butAbout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butAbout.FlatAppearance.BorderSize = 0;
            this.butAbout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.butAbout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butAbout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butAbout.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butAbout.Image = global::WinUp.Properties.Resources.contacto32;
            this.butAbout.Location = new System.Drawing.Point(0, 414);
            this.butAbout.Name = "butAbout";
            this.butAbout.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.butAbout.Size = new System.Drawing.Size(215, 42);
            this.butAbout.TabIndex = 10;
            this.butAbout.Text = "        Acerca de...";
            this.butAbout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butAbout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butAbout.UseVisualStyleBackColor = true;
            this.butAbout.Click += new System.EventHandler(this.butAbout_Click);
            // 
            // butParameters
            // 
            this.butParameters.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butParameters.FlatAppearance.BorderSize = 0;
            this.butParameters.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.butParameters.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butParameters.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butParameters.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butParameters.Image = global::WinUp.Properties.Resources.engranajeAzul24;
            this.butParameters.Location = new System.Drawing.Point(-3, 277);
            this.butParameters.Name = "butParameters";
            this.butParameters.Size = new System.Drawing.Size(215, 42);
            this.butParameters.TabIndex = 9;
            this.butParameters.Text = "       Parámetros";
            this.butParameters.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butParameters.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butParameters.UseVisualStyleBackColor = true;
            this.butParameters.Click += new System.EventHandler(this.butParameters_Click);
            // 
            // button3
            // 
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.button3.Image = global::WinUp.Properties.Resources.cerrar_sesionAzul24;
            this.button3.Location = new System.Drawing.Point(0, 474);
            this.button3.Name = "button3";
            this.button3.Padding = new System.Windows.Forms.Padding(0, 0, 35, 0);
            this.button3.Size = new System.Drawing.Size(215, 42);
            this.button3.TabIndex = 4;
            this.button3.Text = "        Salir";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // butSetup
            // 
            this.butSetup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butSetup.FlatAppearance.BorderSize = 0;
            this.butSetup.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.butSetup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butSetup.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butSetup.Image = global::WinUp.Properties.Resources.enchufeAzul24;
            this.butSetup.Location = new System.Drawing.Point(0, 228);
            this.butSetup.Name = "butSetup";
            this.butSetup.Padding = new System.Windows.Forms.Padding(2, 0, 10, 0);
            this.butSetup.Size = new System.Drawing.Size(215, 42);
            this.butSetup.TabIndex = 3;
            this.butSetup.Text = "       Conexión BD";
            this.butSetup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butSetup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butSetup.UseVisualStyleBackColor = true;
            this.butSetup.Click += new System.EventHandler(this.butSetup_Click);
            // 
            // butUpload
            // 
            this.butUpload.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butUpload.FlatAppearance.BorderSize = 0;
            this.butUpload.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.butUpload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butUpload.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butUpload.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butUpload.Image = global::WinUp.Properties.Resources._subir4;
            this.butUpload.Location = new System.Drawing.Point(0, 130);
            this.butUpload.Name = "butUpload";
            this.butUpload.Size = new System.Drawing.Size(215, 42);
            this.butUpload.TabIndex = 2;
            this.butUpload.Text = "      Procesar datos";
            this.butUpload.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butUpload.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butUpload.UseVisualStyleBackColor = true;
            this.butUpload.Click += new System.EventHandler(this.butUpload_Click);
            // 
            // picLine
            // 
            this.picLine.Image = global::WinUp.Properties.Resources.lineaAzul;
            this.picLine.Location = new System.Drawing.Point(10, 90);
            this.picLine.Name = "picLine";
            this.picLine.Size = new System.Drawing.Size(191, 5);
            this.picLine.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLine.TabIndex = 8;
            this.picLine.TabStop = false;
            // 
            // labelMenu2
            // 
            this.labelMenu2.AutoSize = true;
            this.labelMenu2.Font = new System.Drawing.Font("MV Boli", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMenu2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.labelMenu2.Location = new System.Drawing.Point(73, 47);
            this.labelMenu2.Name = "labelMenu2";
            this.labelMenu2.Size = new System.Drawing.Size(53, 21);
            this.labelMenu2.TabIndex = 7;
            this.labelMenu2.Text = "Betel";
            // 
            // labelMenu1
            // 
            this.labelMenu1.AutoSize = true;
            this.labelMenu1.Font = new System.Drawing.Font("MV Boli", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMenu1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.labelMenu1.Location = new System.Drawing.Point(74, 26);
            this.labelMenu1.Name = "labelMenu1";
            this.labelMenu1.Size = new System.Drawing.Size(106, 21);
            this.labelMenu1.TabIndex = 6;
            this.labelMenu1.Text = "Laboratorio";
            // 
            // picLogo
            // 
            this.picLogo.Image = global::WinUp.Properties.Resources.subirAmarillo;
            this.picLogo.Location = new System.Drawing.Point(14, 23);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(43, 41);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogo.TabIndex = 2;
            this.picLogo.TabStop = false;
            // 
            // panelDesktop
            // 
            this.panelDesktop.Controls.Add(this.panelFoot);
            this.panelDesktop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDesktop.Location = new System.Drawing.Point(215, 42);
            this.panelDesktop.Name = "panelDesktop";
            this.panelDesktop.Size = new System.Drawing.Size(885, 658);
            this.panelDesktop.TabIndex = 2;
            // 
            // panelFoot
            // 
            this.panelFoot.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.panelFoot.Controls.Add(this.butFoot5);
            this.panelFoot.Controls.Add(this.butFoot4);
            this.panelFoot.Controls.Add(this.label1);
            this.panelFoot.Controls.Add(this.labdb);
            this.panelFoot.Controls.Add(this.butFoot3);
            this.panelFoot.Controls.Add(this.butFoot2);
            this.panelFoot.Controls.Add(this.butFoot1);
            this.panelFoot.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFoot.Location = new System.Drawing.Point(0, 628);
            this.panelFoot.Name = "panelFoot";
            this.panelFoot.Size = new System.Drawing.Size(885, 30);
            this.panelFoot.TabIndex = 0;
            this.panelFoot.Paint += new System.Windows.Forms.PaintEventHandler(this.panelFoot_Paint);
            // 
            // butFoot4
            // 
            this.butFoot4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.butFoot4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butFoot4.FlatAppearance.BorderSize = 0;
            this.butFoot4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.butFoot4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butFoot4.Location = new System.Drawing.Point(104, 3);
            this.butFoot4.Name = "butFoot4";
            this.butFoot4.Size = new System.Drawing.Size(25, 25);
            this.butFoot4.TabIndex = 7;
            this.butFoot4.Tag = "1";
            this.butFoot4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butFoot4.UseVisualStyleBackColor = false;
            this.butFoot4.Click += new System.EventHandler(this.butFoot4_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.label1.Location = new System.Drawing.Point(665, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 18);
            this.label1.TabIndex = 6;
            this.label1.Text = "Base de datos: ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // labdb
            // 
            this.labdb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labdb.AutoSize = true;
            this.labdb.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labdb.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.labdb.Location = new System.Drawing.Point(772, 6);
            this.labdb.Name = "labdb";
            this.labdb.Size = new System.Drawing.Size(0, 18);
            this.labdb.TabIndex = 5;
            this.labdb.Click += new System.EventHandler(this.labdb_Click);
            // 
            // butFoot3
            // 
            this.butFoot3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.butFoot3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butFoot3.FlatAppearance.BorderSize = 0;
            this.butFoot3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.butFoot3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butFoot3.Location = new System.Drawing.Point(72, 3);
            this.butFoot3.Name = "butFoot3";
            this.butFoot3.Size = new System.Drawing.Size(25, 25);
            this.butFoot3.TabIndex = 4;
            this.butFoot3.Tag = "1";
            this.butFoot3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butFoot3.UseVisualStyleBackColor = false;
            this.butFoot3.Click += new System.EventHandler(this.butFoot3_Click);
            // 
            // butFoot2
            // 
            this.butFoot2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.butFoot2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butFoot2.FlatAppearance.BorderSize = 0;
            this.butFoot2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.butFoot2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butFoot2.Location = new System.Drawing.Point(40, 3);
            this.butFoot2.Name = "butFoot2";
            this.butFoot2.Size = new System.Drawing.Size(25, 25);
            this.butFoot2.TabIndex = 3;
            this.butFoot2.Tag = "1";
            this.butFoot2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butFoot2.UseVisualStyleBackColor = false;
            this.butFoot2.Click += new System.EventHandler(this.butFoot2_Click);
            // 
            // butFoot1
            // 
            this.butFoot1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.butFoot1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butFoot1.FlatAppearance.BorderSize = 0;
            this.butFoot1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.butFoot1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butFoot1.Location = new System.Drawing.Point(8, 3);
            this.butFoot1.Name = "butFoot1";
            this.butFoot1.Size = new System.Drawing.Size(25, 25);
            this.butFoot1.TabIndex = 2;
            this.butFoot1.Tag = "1";
            this.butFoot1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butFoot1.UseVisualStyleBackColor = false;
            this.butFoot1.Click += new System.EventHandler(this.butFoot1_Click);
            // 
            // butFoot5
            // 
            this.butFoot5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.butFoot5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butFoot5.FlatAppearance.BorderSize = 0;
            this.butFoot5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.butFoot5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butFoot5.Location = new System.Drawing.Point(136, 3);
            this.butFoot5.Name = "butFoot5";
            this.butFoot5.Size = new System.Drawing.Size(25, 25);
            this.butFoot5.TabIndex = 8;
            this.butFoot5.Tag = "1";
            this.butFoot5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butFoot5.UseVisualStyleBackColor = false;
            this.butFoot5.Click += new System.EventHandler(this.butFoot5_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(171)))), ((int)(((byte)(171)))));
            this.ClientSize = new System.Drawing.Size(1100, 700);
            this.Controls.Add(this.panelDesktop);
            this.Controls.Add(this.panelMenu);
            this.Controls.Add(this.panelTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Upload File";
            this.Load += new System.EventHandler(this.fmMain_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMain_KeyDown);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.frmMain_MouseClick);
            this.panelTop.ResumeLayout(false);
            this.panelMenu.ResumeLayout(false);
            this.panelMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.panelDesktop.ResumeLayout(false);
            this.panelFoot.ResumeLayout(false);
            this.panelFoot.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Button butExit;
        private System.Windows.Forms.Button butMaximize;
        private System.Windows.Forms.Button butMinimize;
        private System.Windows.Forms.Button butRestore;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.Label labelMenu1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labelMenu2;
        private System.Windows.Forms.PictureBox picLine;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel panelDesktop;
        private System.Windows.Forms.Panel panelFoot;
        public System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Button butUpload;
        private System.Windows.Forms.Button butParameters;
        internal System.Windows.Forms.Button butSetup;
        private System.Windows.Forms.Button butFoot1;
        private System.Windows.Forms.Button butFoot3;
        private System.Windows.Forms.Button butFoot2;
        private System.Windows.Forms.Label labdb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button butAbout;
        private System.Windows.Forms.Button butFoot4;
        private System.Windows.Forms.Button butSeries;
        private System.Windows.Forms.Button butFoot5;
    }
}

