﻿using Business_Up;
using Entity_Up;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;


namespace WinUp
{
    public partial class frmConection : Form
    {

        public frmConection()
        {
            InitializeComponent();
        }

        public IFormMain frmMain { get; set; }

        private int posY = 0;
        private int posX = 0;

        private XmlDocument doc;
        private string nodoRaiz = "ProbiaConfigurations";
        private string route = Application.StartupPath + @"\Probia.Config";
        private string xlmname = Application.StartupPath + @"\Probia.Config\Probia.Configuration.xml";
        private string xscript = Application.StartupPath + @"\Probia.Config\MasterProbiaJR.sql";
        private List<string> _name = new List<String>();
        private List<string> _values = new List<String>();

        private string _txtServer = "";
        private string _txtDatabase = "";
        private string _txtUser = "";
        private string _txtPassword = "";
        private string _txtServer2 = "";
        private string _txtDatabase2 = "";
        private string _txtUser2 = "";
        private string _txtPassword2 = "";

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            MovePanel(e);
        }

        private void frmSetup_Load(object sender, EventArgs e)
        {
            _name.Add("ServerName");
            _name.Add("bdname");
            _name.Add("user");
            _name.Add("password");
            _name.Add("ServerName2");
            _name.Add("bdname2");
            _name.Add("user2");
            _name.Add("password2");

            if (File.Exists(xlmname))
            {
                ReadXml(xlmname);
            }

            txtServer.Focus();
        }

        private void MovePanel(MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                posX = e.X;
                posY = e.Y;
            }
            else
            {
                Left = Left + (e.X - posX);
                Top = Top + (e.Y - posY);
            }
        }

        private void butExit_Click(object sender, EventArgs e)
        {
            frmMain form = new frmMain();
            form.SetbutSetup = Color.FromArgb(221, 234, 239);
            
            this.Close();
        }

        private void butTest_Click(object sender, EventArgs e)
        {
            if (!ValidFields()) return;

            E_Resp resp = TestConection(1);
            if (resp.Status)
            {
                MessageBox.Show("Conexión establecida satisfatoriamente PROFIT PLUS",
                                "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            else
            {
                MessageBox.Show("No se pudo establecer conexión con la Base de Datos PROFIT PLUS. \n" + resp.errorMessage,
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void butTest2_Click(object sender, EventArgs e)
        {
            if (!ValidFields()) return;

            E_Resp resp = TestConection(2);
            if (resp.Status)
            {
                MessageBox.Show("Conexión establecida satisfatoriamente INFOLAB",
                                "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            else
            {
                MessageBox.Show("No se pudo establecer conexión con la Base de Datos INFOLAB. \n" + resp.errorMessage,
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

        }

        private E_Resp TestConection(int serv)
        {
            Cursor.Current = Cursors.WaitCursor;
            E_Resp resp = new E_Resp();
            Cursor.Current = Cursors.WaitCursor;

            if (serv == 1)
            {
                resp = DataConection.TestConection(txtServer.Text.Trim(), txtDatabase.Text.TrimEnd(),
                                                   txtUser.Text.TrimEnd(), txtPassword.Text.TrimEnd(), "");
            }
            else
            {
                resp = DataConection.TestConection(txtServer2.Text.Trim(), txtDatabase2.Text.TrimEnd(),
                                                   txtUser2.Text.TrimEnd(), txtPassword2.Text.TrimEnd(), "");
            }

            Cursor.Current = Cursors.Default;
            return resp;
        }

        private bool ValidFields()
        {
            if (txtServer.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtServer, "Debe ingresar el nombre de la instancia o servidor sql server.");
                txtServer.Focus();
                return false;
            }
            errorProvider1.SetError(txtServer, "");
            if (txtServer2.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtServer2, "Debe ingresar el nombre de la instancia o servidor sql server.");
                txtServer2.Focus();
                return false;
            }
            errorProvider1.SetError(txtServer2, "");

            if (txtDatabase.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtDatabase, "Debe ingresar el nombre de la base de datos");
                txtDatabase.Focus();
                return false;
            }
            errorProvider1.SetError(txtDatabase, "");
            if (txtDatabase2.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtDatabase2, "Debe ingresar el nombre de la base de datos");
                txtDatabase2.Focus();
                return false;
            }
            errorProvider1.SetError(txtDatabase2, "");

            if (txtUser.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtUser, "Debe ingresar el usuario con permisos a base de datos");
                txtUser.Focus();
                return false;
            }
            errorProvider1.SetError(txtUser, "");
            if (txtUser2.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtUser2, "Debe ingresar el usuario con permisos a base de datos");
                txtUser2.Focus();
                return false;
            }
            errorProvider1.SetError(txtUser2, "");

            if (txtPassword.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtPassword, "Debe ingresar la contraseña del usuario");
                txtPassword.Focus();
                return false;
            }
            errorProvider1.SetError(txtPassword, "");
            if (txtPassword2.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtPassword2, "Debe ingresar la contraseña del usuario");
                txtPassword2.Focus();
                return false;
            }
            errorProvider1.SetError(txtPassword2, "");

            return true;
        }

        private void butSave_Click(object sender, EventArgs e)
        {
            if (!ValidFields()) return;
            E_Resp resp = TestConection(1);
            if (!resp.Status)
            {
                MessageBox.Show("No se pudo establecer conexión con la Base de Datos PROFIT PLUS. \n" + resp.errorMessage,
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return;
            }
            resp = TestConection(2);
            if (!resp.Status)
            {
                MessageBox.Show("No se pudo establecer conexión con la Base de Datos INFOLAB. \n" + resp.errorMessage,
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return;
            }

            try
            {
                if (File.Exists(xlmname))
                {
                    updateXml();
                    MessageBox.Show("Conexión modificada satisfactoriamente!", "información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                else
                {
                    CreateFolder(route);
                    CreateXml(xlmname, nodoRaiz);
                    MessageBox.Show("Conexión guardada satisfactoriamente!", "información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                _txtServer = txtServer.Text;
                _txtDatabase = txtDatabase.Text;
                _txtUser = txtUser.Text;
                _txtPassword = txtPassword.Text;
                _txtServer2 = txtServer2.Text;
                _txtDatabase2 = txtDatabase2.Text;
                _txtUser2 = txtUser2.Text;
                _txtPassword2 = txtPassword2.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido una inconsistencia al modificar el archivo de configuración \n " +
                                "Por favor contacte su administrador de sistema\n \n" + ex.Message,
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void updateXml()
        {
            var root = new XmlDocument();
            root.Load(xlmname);
            XmlNode node = root.SelectSingleNode("ProbiaConfigurations/MasterConectionString");
            node["ServerName"].InnerText = txtServer.Text;
            node["bdname"].InnerText = txtDatabase.Text;
            node["user"].InnerText = txtUser.Text;
            node["password"].InnerText = txtPassword.Text;
            node["ServerName2"].InnerText = txtServer2.Text;
            node["bdname2"].InnerText = txtDatabase2.Text;
            node["user2"].InnerText = txtUser2.Text;
            node["password2"].InnerText = txtPassword2.Text;
            root.Save(xlmname);
        }

        private void CreateFolder(string foldername)
        {
            try
            {
                if (!Directory.Exists(foldername))
                {
                    Directory.CreateDirectory(foldername);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido una inconsistencia al crear la carpeta: <Probia.Config> \n " +
                                "Por favor contacte su administrador de sistema\n \n" + ex.Message,
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void CreateXml(string route, string nodoroot)
        {
            try
            {
                // Create XML + Principal Node
                doc = new XmlDocument();
                XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                XmlNode root = doc.DocumentElement;
                doc.InsertBefore(xmlDeclaration, root);
                XmlNode element1 = doc.CreateElement(nodoroot);
                doc.AppendChild(element1);
                doc.Save(route);

                // Create Node y subnode
                _values.Add(txtServer.Text.TrimEnd());
                _values.Add(txtDatabase.Text.TrimEnd());
                _values.Add(txtUser.Text.TrimEnd());
                _values.Add(txtPassword.Text.TrimEnd());
                _values.Add(txtServer2.Text.TrimEnd());
                _values.Add(txtDatabase2.Text.TrimEnd());
                _values.Add(txtUser2.Text.TrimEnd());
                _values.Add(txtPassword2.Text.TrimEnd());

                CreateNode(xlmname, "MasterConectionString", _name, _values);
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo crear el archivo .XML: <Probia.Config> \n " +
                                "Por favor contacte su administrador de sistema\n \n" + ex.Message,
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void CreateNode(string xlmname, string nodoname, List<string> _name, List<string> _values)
        {
            try
            {
                // Create Node
                doc = new XmlDocument();
                doc.Load(xlmname);

                XmlNode xNode = CreateSubNode(nodoname, _name, _values);
                XmlNode nodoroot = doc.DocumentElement;
                nodoroot.InsertAfter(xNode, nodoroot.LastChild);
                doc.Save(xlmname);
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo crear el archivo .XML: <Probia.Config> \n " +
                                "Por favor contacte su administrador de sistema\n \n" + ex.Message,
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private XmlNode CreateSubNode(string nodoname, List<string> _name, List<string> _values)
        {
            XmlNode nodename = doc.CreateElement(nodoname);
            int i = 0;
            foreach (var item in _name)
            {
                XmlElement xsubnode = doc.CreateElement(item.ToString());
                xsubnode.InnerText = _values[i].ToString();
                nodename.AppendChild(xsubnode);
                i++;
            }
            return nodename;
        }

        public void ReadXml(string xlmname)
        {
            doc = new XmlDocument();
            doc.Load(xlmname);
            XmlNodeList route = doc.SelectNodes("ProbiaConfigurations/MasterConectionString");
            XmlNode subnode = route.Item(0); 
            txtServer.Text = subnode.SelectSingleNode("ServerName").InnerText;
            _txtServer = subnode.SelectSingleNode("ServerName").InnerText;
            txtDatabase.Text = subnode.SelectSingleNode("bdname").InnerText;
            _txtDatabase = subnode.SelectSingleNode("bdname").InnerText;
            txtUser.Text = subnode.SelectSingleNode("user").InnerText;
            _txtUser = subnode.SelectSingleNode("user").InnerText;
            txtPassword.Text = subnode.SelectSingleNode("password").InnerText;
            _txtPassword = subnode.SelectSingleNode("password").InnerText;
            // INFOLAB
            txtServer2.Text = subnode.SelectSingleNode("ServerName2").InnerText;
            _txtServer2 = subnode.SelectSingleNode("ServerName2").InnerText;
            txtDatabase2.Text = subnode.SelectSingleNode("bdname2").InnerText;
            _txtDatabase2 = subnode.SelectSingleNode("bdname2").InnerText;
            txtUser2.Text = subnode.SelectSingleNode("user2").InnerText;
            _txtUser2 = subnode.SelectSingleNode("user2").InnerText;
            txtPassword2.Text = subnode.SelectSingleNode("password2").InnerText;
            _txtPassword2 = subnode.SelectSingleNode("password2").InnerText;
        }

        private void butMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
            this.frmMain.ChangeImageFoot(picIcon.Image, "3");
            this.frmMain.backColorButton();
        }

        private void frmConection_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(_txtServer != txtServer.Text || _txtDatabase != txtDatabase.Text ||
               _txtUser != txtUser.Text || _txtPassword != txtPassword.Text ||
               _txtServer2 != txtServer2.Text || _txtDatabase2 != txtDatabase2.Text ||
               _txtUser2 != txtUser2.Text || _txtPassword2 != txtPassword2.Text)
            {
                DialogResult rta = MessageBox.Show("¿Desea de abandonar cambios?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (rta == DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }
            this.frmMain.backColorButton();
        }

        private void frmConection_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void frmConection_MouseClick(object sender, MouseEventArgs e)
        {
            if (ActiveMdiChild == null)
            {
                this.BringToFront();
                this.frmMain.backColorButton();
                this.frmMain.frontColorButton(2);
            }
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            frmConection_MouseClick(null, null);
        }

        
    }
}
