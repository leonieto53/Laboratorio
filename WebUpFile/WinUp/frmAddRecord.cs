﻿using Business_Up;
using Entity_Up;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace WinUp
{
    public partial class frmAddRecord : Form
    {

        public frmAddRecord()
        {
            InitializeComponent();
        }

        private int posY = 0;
        private int posX = 0;

        private DateTime _dtpFecha = DateTime.Now;
        private string _txtSerie = string.Empty;
        private string _txtControl = string.Empty;
        private string _txtFactura = string.Empty;
        private string _txtRif = string.Empty;
        private string _txtAnulada = string.Empty;
        private string _txtNombre = string.Empty;
        private string _txtConvenio = string.Empty;
        private string _txtSubtotal = string.Empty;
        private string _txtTotaliva = string.Empty;
        private string _txtSucursal = string.Empty;

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            MovePanel(e);
        }

        private void MovePanel(MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                posX = e.X;
                posY = e.Y;
            }
            else
            {
                Left = Left + (e.X - posX);
                Top = Top + (e.Y - posY);
            }
        }

        private void frmSetup_Load(object sender, EventArgs e)
        {
            txtTotaliva.Text = "0";
            SetFields();
            dtpFecha.Focus();

        }

        private void SetFields()
        {
            _dtpFecha = dtpFecha.Value;
            _txtSerie = txtSerie.Text;
            _txtControl = txtControl.Text;
            _txtFactura = txtFactura.Text;
            _txtRif = txtRif.Text;
            _txtAnulada = txtAnulada.Text;
            _txtNombre = txtNombre.Text;
            _txtConvenio = txtConvenio.Text;
            _txtSubtotal = txtSubtotal.Text;
            _txtTotaliva = txtTotaliva.Text;
            _txtSucursal = txtSucursal.Text;
        }

        private void butExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void butSave_Click(object sender, EventArgs e)
        {
            if (!ValidFields()) return;

            try
            {
                List<SqlParameter> prm = new List<SqlParameter>()
                {
                    new SqlParameter("@fec_factura", dtpFecha.Value),
                    new SqlParameter("@numero_serie", txtSerie.Text.Trim()),
                    new SqlParameter("@factura_impresa", txtFactura.Text.Trim()),
                    new SqlParameter("@rif", txtRif.Text.Trim()),
                    new SqlParameter("@codigo_cliente", System.Data.SqlTypes.SqlString.Null),
                    new SqlParameter("@id_convenio", txtConvenio.Text.Trim()),
                    new SqlParameter("@nombre_cliente", txtNombre.Text.Trim()),
                    new SqlParameter("@factura_info", txtFactura.Text.Trim()),
                    new SqlParameter("@numero_control", txtControl.Text.Trim()),
                    new SqlParameter("@subtotal_factura", Convert.ToDecimal(txtSubtotal.Text.Trim())),
                    new SqlParameter("@total_iva", Convert.ToDecimal(txtTotaliva.Text.Trim())),
                    new SqlParameter("@total_factura", Convert.ToDecimal(txtSubtotal.Text.Trim())+Convert.ToDecimal(txtTotaliva.Text.Trim())),
                    new SqlParameter("@alicuota_iva", System.Data.SqlTypes.SqlDecimal.Null),
                    new SqlParameter("@moneda", System.Data.SqlTypes.SqlString.Null),
                    new SqlParameter("@estatus_factura", txtAnulada.Text.Trim() == string.Empty ? "C" : txtAnulada.Text.Trim()),
                    new SqlParameter("@procesado_profit", System.Data.SqlTypes.SqlString.Null),
                    new SqlParameter("@sucursal", txtSucursal.Text.Trim()),
                    new SqlParameter("@fecha_actualizacion", System.Data.SqlTypes.SqlDateTime.Null)
                };

                E_Resp facDT = DataConection.InsertRecord("pInsertarFila", 3, prm);
                if (!facDT.Status)
                {
                    MessageBox.Show("No se pudo insertar el registro \n" + facDT.errorMessage,
                         "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    Cursor.Current = Cursors.Default;
                    return;
                }
                else
                {
                    MessageBox.Show("Registro se guardó exitosamente!","Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    SetFields();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se guardo el registro \n "  + ex.Message,
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private bool ValidFields()
        {
            if (txtSerie.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtSerie, "Debe ingresar los digitos para la serie.");
                txtSerie.Focus();
                return false;
            }
            errorProvider1.SetError(txtSerie, "");

            if (txtControl.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtControl, "Debe ingresar los digitos para el nro. de control.");
                txtControl.Focus();
                return false;
            }
            errorProvider1.SetError(txtControl, "");

            if (txtFactura.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtFactura, "Debe ingresar los digitos para el nro. de factura.");
                txtFactura.Focus();
                return false;
            }
            errorProvider1.SetError(txtFactura, "");

            if (txtRif.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtRif, "Debe ingresar el Rif para la factura");
                txtRif.Focus();
                return false;
            }
            errorProvider1.SetError(txtRif, "");

            if (txtAnulada.Text.TrimEnd() != string.Empty && txtAnulada.Text.TrimEnd() != "A")
            {
                errorProvider1.SetError(txtAnulada, "Debe ingresar: (A) Si la factura está anulada o dejar campo vacio. ");
                txtAnulada.Focus();
                return false;
            }
            errorProvider1.SetError(txtAnulada, "");

            if (txtNombre.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtNombre, "Debe ingresar nombre del responsable.");
                txtNombre.Focus();
                return false;
            }
            errorProvider1.SetError(txtNombre, "");

            if (txtConvenio.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtConvenio, "Debe ingresar id del convenio.");
                txtConvenio.Focus();
                return false;
            }
            errorProvider1.SetError(txtConvenio, "");

            if (txtSubtotal.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtSubtotal, "El campo subtotal esta vacio");
                txtSubtotal.Focus();
                return false;
            }
            else if (!IsDecimal(txtSubtotal.Text.TrimEnd()))
            {
                errorProvider1.SetError(txtSubtotal, "El formato es invalido.");
                txtSubtotal.Focus();
                return false;
            }
            else if (Convert.ToDecimal(txtSubtotal.Text.TrimEnd()) == 0)
            {
                errorProvider1.SetError(txtSubtotal, "EL campo no debe ser cero");
                txtSubtotal.Focus();
                return false;
            }
            errorProvider1.SetError(txtSubtotal, "");

            if (!IsDecimal(txtTotaliva.Text.TrimEnd()))
            {
                errorProvider1.SetError(txtTotaliva, "El formato es invalido.");
                txtTotaliva.Focus();
                return false;
            }
            errorProvider1.SetError(txtTotaliva, "");

            if (txtTotaliva.Text.TrimEnd() == string.Empty)
            {
                txtTotaliva.Text = string.Format("{0:#,##0.00}", double.Parse("0"));
            }

            if (txtSucursal.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtSucursal, "Debe ingresar id de la sucursal.");
                txtSucursal.Focus();
                return false;
            }
            errorProvider1.SetError(txtSucursal, "");


            return true;
        }

        private bool IsDecimal(string txt)
        {
            if (txt == string.Empty)
                return true;

            decimal value;
            if (decimal.TryParse(txt, out value))
            {
                return true;
            }
            return false;
        }

        private void frmParameters_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_txtSerie != txtSerie.Text || _dtpFecha.Date != dtpFecha.Value.Date ||
               _txtControl != txtControl.Text || _txtFactura != txtFactura.Text ||
               _txtRif != txtRif.Text || _txtAnulada != txtAnulada.Text ||
               _txtNombre != txtNombre.Text || _txtConvenio != txtConvenio.Text ||
               _txtSubtotal != txtSubtotal.Text || _txtTotaliva != txtTotaliva.Text ||
               _txtSucursal != txtSucursal.Text)
            {
                DialogResult rta = MessageBox.Show("¿Desea de abandonar cambios?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (rta == DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }
        }

        private void frmParameters_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txtSerie_Enter(object sender, EventArgs e)
        {
            lineshapeColor();
            Color color = Color.MediumVioletRed;
            TextBox txt = sender as TextBox;
            if(txt.Name == "txtSerie")
                lineShape1.BorderColor = color;
            if (txt.Name == "txtControl")
                lineShape2.BorderColor = color;
            if (txt.Name == "txtFactura")
                lineShape3.BorderColor = color;
            if (txt.Name == "txtRif")
                lineShape4.BorderColor = color;
            if (txt.Name == "txtAnulada")
                lineShape5.BorderColor = color;
            if (txt.Name == "txtNombre")
                lineShape6.BorderColor = color;
            if (txt.Name == "txtConvenio")
                lineShape7.BorderColor = color;
            if (txt.Name == "txtSubtotal")
                lineShape8.BorderColor = color;
            if (txt.Name == "txtTotaliva")
                lineShape9.BorderColor = color;
            if (txt.Name == "txtSucursal")
                lineShape10.BorderColor = color;

        }

        private void lineshapeColor()
        {
            Color color = Color.FromArgb(22,79,116);
            lineShape1.BorderColor = color;
            lineShape2.BorderColor = color;
            lineShape3.BorderColor = color;
            lineShape4.BorderColor = color;
            lineShape5.BorderColor = color;
            lineShape6.BorderColor = color;
            lineShape7.BorderColor = color;
            lineShape8.BorderColor = color;
            lineShape9.BorderColor = color;
            lineShape10.BorderColor = color;
        }

        private void butSave_Enter(object sender, EventArgs e)
        {
            lineshapeColor();
        }

        private void txtSubtotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || Char.IsControl(e.KeyChar) || Char.IsPunctuation(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;

            return;
        }

        private void FormatoMoneda(TextBox xTBox)
        {
            if (xTBox.Text == string.Empty || !IsDecimal(xTBox.Text))
                return;
            else
            {
                decimal Monto;
                Monto = Convert.ToDecimal(xTBox.Text);
                xTBox.Text = string.Format("{0:#,##0.00}", double.Parse(xTBox.Text));
            }
        }

        private void txtSubtotal_Leave(object sender, EventArgs e)
        {
            TextBox txt = sender as TextBox;
            FormatoMoneda(txt);
        }
    }
}
