﻿using Business_Up;
using Entity_Up;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace WinUp
{
    public partial class frmAddRecordsSerie : Form
    {

        public frmAddRecordsSerie()
        {
            InitializeComponent();
        }

        private int posY = 0;
        private int posX = 0;

        private string _txtCodigoSerie = string.Empty;
        private string _txtDescription = string.Empty;

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            MovePanel(e);
        }

        private void MovePanel(MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                posX = e.X;
                posY = e.Y;
            }
            else
            {
                Left = Left + (e.X - posX);
                Top = Top + (e.Y - posY);
            }
        }

        private void frmSetup_Load(object sender, EventArgs e)
        {
            SetFields();
            txtCodigoSerie.Focus();
        }

        private void SetFields()
        {
            _txtCodigoSerie = txtCodigoSerie.Text;
            _txtDescription = txtDescription.Text;
        }

        private void butExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void butSave_Click(object sender, EventArgs e)
        {
            if (!ValidFields()) return;

            try
            {
                List<SqlParameter> prm = new List<SqlParameter>()
                {
                    new SqlParameter("@IdSerie", txtCodigoSerie.Text.Trim()),
                    new SqlParameter("@Description", txtDescription.Text.Trim()),
                };

                E_Resp serieDT = DataConection.InsertRecord("pInsertarSerie", 3, prm);
                if (!serieDT.Status)
                {
                    MessageBox.Show("No se pudo insertar el registro \n" + serieDT.errorMessage,
                         "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    Cursor.Current = Cursors.Default;
                    return;
                }
                else
                {
                    MessageBox.Show("Registro se guardó exitosamente!","Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    SetFields();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se guardo el registro \n "  + ex.Message,
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private bool ValidFields()
        {
            if (txtCodigoSerie.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtCodigoSerie, "Debe ingresar el codigo para la serie.");
                txtCodigoSerie.Focus();
                return false;
            }
            errorProvider1.SetError(txtCodigoSerie, "");

            if (txtDescription.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtDescription, "Debe ingresar la descrición para la serie.");
                txtDescription.Focus();
                return false;
            }
            errorProvider1.SetError(txtDescription, "");

            return true;
        }

        private void frmParameters_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_txtCodigoSerie != txtCodigoSerie.Text || _txtDescription != txtDescription.Text)
            {
                DialogResult rta = MessageBox.Show("¿Desea de abandonar cambios?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (rta == DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }
        }

        private void frmParameters_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txtSerie_Enter(object sender, EventArgs e)
        {
            lineshapeColor();
            Color color = Color.MediumVioletRed;
            TextBox txt = sender as TextBox;
            if(txt.Name == "txtCodigoSerie")
                lineShape1.BorderColor = color;
            if (txt.Name == "txtDescription")
                lineShape6.BorderColor = color;
        }

        private void lineshapeColor()
        {
            Color color = Color.FromArgb(22,79,116);
            lineShape1.BorderColor = color;
            lineShape6.BorderColor = color;
        }

        private void butSave_Enter(object sender, EventArgs e)
        {
            lineshapeColor();
        }


    }
}
