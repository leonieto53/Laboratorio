﻿using Business_Up;
using Entity_Up;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace WinUp
{
    public partial class frmMain : Form, IFormMain
    {

        public frmMain()
        {
            //IFormMain _text;
            InitializeComponent();
        }

        private string xlmconection = Application.StartupPath + @"\Probia.Config\Probia.Configuration.xml";
        private string xlmparameter = Application.StartupPath + @"\Probia.Config\Probia.Parameters.xml";

        public Color SetbutSetup
        {
            set
            {
                butSetup.BackColor = value;
            }
        }

        private Form formActive = null;
                private int posY = 0;
        private int posX = 0;

        private void butExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void butRestore_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
            butRestore.Visible = false;
            butMaximize.Visible = true;
        }

        private void butMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void butMaximize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;
            butMaximize.Visible = false;
            butRestore.Visible = true;
        }

        private void MovePanel(MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                posX = e.X;
                posY = e.Y;
            }
            else
            {
                Left = Left + (e.X - posX);
                Top = Top + (e.Y - posY);
            }
        }


        private void panelTop_MouseMove(object sender, MouseEventArgs e)
        {
            MovePanel(e);
        }

        public void ScreenOk()
        {
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
        }

        private void fmMain_Load(object sender, EventArgs e)
        {
            ScreenOk();
            E_XmlFields field = DataConection.ReadXml();
            if (field.ok)
            {
                labdb.Text = field.c_database;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (panelMenu.Width == 215)
            {
                //Sidebar.Visible = false;
                panelMenu.Width = 68;
                labelMenu1.Visible = false;
                labelMenu2.Visible = false;
                picLine.Width = 48;
            }
            else
            {
                panelMenu.Width = 215;
                labelMenu1.Visible = true;
                labelMenu2.Visible = true;
                picLine.Width = 191;
            }
        }

        public void backColorButton()
        {
            butUpload.BackColor = Color.FromArgb(221, 234, 239);
            butSeries.BackColor = Color.FromArgb(221, 234, 239);
            butSetup.BackColor = Color.FromArgb(221, 234, 239);
            butParameters.BackColor = Color.FromArgb(221, 234, 239);
            butAbout.BackColor = Color.FromArgb(221, 234, 239);
        }

        public void frontColorButton(int but)
        {
            if(but == 1) 
                butUpload.BackColor = Color.FromArgb(255, 223, 130);
            if (but == 2)
                butSeries.BackColor = Color.FromArgb(255, 223, 130);
            if (but == 3)
                butSetup.BackColor = Color.FromArgb(255, 223, 130);
            if (but == 4)
                butParameters.BackColor = Color.FromArgb(255, 223, 130);
            if (but == 5)
                butAbout.BackColor = Color.FromArgb(255, 223, 130);
        }


        private void butUpload_Click(object sender, EventArgs e)
        {

            backColorButton();
            butUpload.BackColor = Color.FromArgb(255, 223, 130);

            if (Share.CheckForm<frmUpload>())
            {
                butFoot1.Image = null;
                butFoot1.Tag = null;
                return;
            }

            if (!File.Exists(xlmconection))
            {
                MessageBox.Show("No se han establecido los parámetros de conexiones con la base de datos.",
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return;
            }
            if (!File.Exists(xlmparameter))
            {
                MessageBox.Show("No se han establecido los parámetros para los campos por defecto.",
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return;
            }

            frmUpload form2 = new frmUpload();
            formActive = form2;
            form2.TopLevel = false;
            panelDesktop.Controls.Add(form2);
            panelDesktop.Tag = form2;
            //form2.MdiParent = this.MdiParent;
            form2.frmMain = this;
            form2.BringToFront();
            form2.Show();

            //OpenFormChild(new frmUpload());
        }

        private void butSetup_Click(object sender, EventArgs e)
        {
            backColorButton();
            butSetup.BackColor = Color.FromArgb(255, 223, 130);

            if (Share.CheckForm<frmConection>())
            {
                butFoot3.Image = null;
                butFoot3.Tag = null;
                return;
            }

            //OpenFormChild(new frmConection());
            frmConection form2 = new frmConection();
            formActive = form2;
            form2.TopLevel = false;
            panelDesktop.Controls.Add(form2);
            panelDesktop.Tag = form2;
            //form2.MdiParent = this.MdiParent;
            form2.frmMain = this;
            form2.BringToFront();
            form2.Show();
        }

        private void OpenFormChild(Form formchild)
        {
            //if (formActive != null)
            //    formActive.Close();

            formActive = formchild;
            formchild.TopLevel = false;
            //formchild.Dock = DockStyle.Fill;
            panelDesktop.Controls.Add(formchild);
            panelDesktop.Tag = formchild;
            formchild.BringToFront();
            formchild.Show();
        }

        private void butParameters_Click(object sender, EventArgs e)
        {
            backColorButton();
            butParameters.BackColor = Color.FromArgb(255, 223, 130);

            if (Share.CheckForm<frmContact>())
            {
                butFoot4.Image = null;
                butFoot4.Tag = null;
                return;
            }
               
            if (!File.Exists(xlmconection))
            {
                MessageBox.Show("No se han creado las conexiones con la base de datos",
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return;
            }

            //OpenFormChild(new frmParameters());
            frmContact form2 = new frmContact();
            formActive = form2;
            form2.TopLevel = false;
            panelDesktop.Controls.Add(form2);
            panelDesktop.Tag = form2;
            //form2.MdiParent = this.MdiParent;
            form2.frmMain = this;
            form2.BringToFront();
            form2.Show();

        }



        public void ChangeImageFoot(Image image, string text)
        {
            if (text == "1")
            {
                butFoot1.Image = image;
                butFoot1.Tag = text;
            }
            if (text == "2")
            {
                butFoot2.Image = image;
                butFoot2.Tag = text;
            }
            if (text == "3")
            {
                butFoot3.Image = image;
                butFoot3.Tag = text;
            }
            if (text == "4")
            {
                butFoot4.Image = image;
                butFoot4.Tag = text;
            }
            if (text == "5")
            {
                butFoot5.Image = image;
                butFoot5.Tag = text;
            }
        }

        private void butFoot1_Click(object sender, EventArgs e)
        {
            if (butFoot1.Image != null)
            {
                Share.CheckForm<frmUpload>();
                frmUpload frm = new frmUpload();
                frm.BringToFront();
                butFoot1.Image = null;
                butFoot1.Tag = null;
                backColorButton();
                butUpload.BackColor = Color.FromArgb(255, 223, 130);
            }
            
        }

        private void butFoot2_Click(object sender, EventArgs e)
        {
            if (butFoot2.Image != null)
            {
                Share.CheckForm<frmSeries>();
                butFoot2.Image = null;
                butFoot2.Tag = null;
                backColorButton();
                butSeries.BackColor = Color.FromArgb(255, 223, 130);
            }
        }

        private void butFoot3_Click(object sender, EventArgs e)
        {
            if (butFoot3.Image != null)
            {
                Share.CheckForm<frmConection>();
                butFoot3.Image = null;
                butFoot3.Tag = null;
                backColorButton();
                butSetup.BackColor = Color.FromArgb(255, 223, 130);
            }
        }

        private void frmMain_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Escape)
            //{
                
            //}
        }

        private void frmMain_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void panelFoot_Paint(object sender, PaintEventArgs e)
        {

        }

        private void labdb_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void butAbout_Click(object sender, EventArgs e)
        {
            backColorButton();
            butAbout.BackColor = Color.FromArgb(255, 223, 130);

            if (Share.CheckForm<frmAbout>())
            {
                butFoot5.Image = null;
                butFoot5.Tag = null;
                return;
            }

            frmAbout form2 = new frmAbout();
            formActive = form2;
            form2.TopLevel = false;
            panelDesktop.Controls.Add(form2);
            panelDesktop.Tag = form2;
            form2.frmMain = this;
            form2.BringToFront();
            form2.Show();
        }

        private void butFoot4_Click(object sender, EventArgs e)
        {
            if (butFoot4.Image != null)
            {
                Share.CheckForm<frmContact>();
                butFoot4.Image = null;
                butFoot4.Tag = null;
                backColorButton();
                butParameters.BackColor = Color.FromArgb(255, 223, 130);
            }
        }

        private void butSeries_Click(object sender, EventArgs e)
        {
            backColorButton();
            butSeries.BackColor = Color.FromArgb(255, 223, 130);

            if (Share.CheckForm<frmSeries>())
            {
                butFoot2.Image = null;
                butFoot2.Tag = null;
                return;
            }

            //OpenFormChild(new frmConection());
            frmSeries form2 = new frmSeries();
            formActive = form2;
            form2.TopLevel = false;
            panelDesktop.Controls.Add(form2);
            panelDesktop.Tag = form2;
            //form2.MdiParent = this.MdiParent;
            form2.frmMain = this;
            form2.BringToFront();
            form2.Show();
        }

        private void butFoot5_Click(object sender, EventArgs e)
        {
            if (butFoot5.Image != null)
            {
                Share.CheckForm<frmAbout>();
                butFoot5.Image = null;
                butFoot5.Tag = null;
                backColorButton();
                butAbout.BackColor = Color.FromArgb(255, 223, 130);
            }

        }
    }
}
