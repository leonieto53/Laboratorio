﻿using Business_Up;
using Entity_Up;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace WinUp
{
    public partial class frmContact : Form
    {

        public frmContact()
        {
            InitializeComponent();
        }

        public IFormMain frmMain { get; set; }

        private int posY = 0;
        private int posX = 0;

        private XmlDocument doc;
        private string nodoRaiz = "ProbiaParameters";
        private string route = Application.StartupPath + @"\Probia.Config";
        private string xlmname = Application.StartupPath + @"\Probia.Config\Probia.Parameters.xml";
        private List<string> _name = new List<String>();
        private List<string> _values = new List<String>();

        private string _txtTransporte = "";
        private string _txtAlmacen = "";
        private string _txtTipoPrecio = "";
        private string _txtIva = "";
        private string _txtUser = "";
        private string _txtMoneda = "";
        private string _txtPrincipal = "";
        private string _txtCustomer = "";
        private string _txtSucursal = "";
        private string _txtArtP = "";
        private string _txtArtS = "";

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            MovePanel(e);
        }

        private void MovePanel(MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                posX = e.X;
                posY = e.Y;
            }
            else
            {
                Left = Left + (e.X - posX);
                Top = Top + (e.Y - posY);
            }
        }

        private void frmSetup_Load(object sender, EventArgs e)
        {
            _name.Add("Transport");
            _name.Add("Warehouse");
            _name.Add("PriceType");
            _name.Add("IvaType");
            _name.Add("User");
            _name.Add("Moneda");
            _name.Add("Principal");
            _name.Add("Customer");
            _name.Add("Sucursal");
            _name.Add("ArtP");
            _name.Add("ArtS");

            try
            {
                if (File.Exists(xlmname))
                {
                    ReadXml(xlmname);
                }
                txtTransporte.Focus();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Ha ocurrido un error al ubicar los archivos de configuración \n " + ex.Message,
                                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                this.Close();
            }
            
        }

        private void butExit_Click(object sender, EventArgs e)
        {
            frmMain form = new frmMain();
            form.SetbutSetup = Color.FromArgb(221, 234, 239);
            this.Close();
        }

        private void butSave_Click(object sender, EventArgs e)
        {
            if (!ValidFields()) return;
            if (!ValidDataBases()) return;


            try
            {
                if (File.Exists(xlmname))
                {
                    updateXml();
                    MessageBox.Show("Conexión modificada satisfactoriamente!", "información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                else
                {
                    CreateFolder(route);
                    CreateXml(xlmname, nodoRaiz);
                    MessageBox.Show("Parametros guardados satisfactoriamente!", "información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                _txtTransporte = txtTransporte.Text;
                _txtAlmacen = txtAlmacen.Text;
                _txtTipoPrecio = txtTipoPrecio.Text;
                _txtIva = txtIva.Text;
                _txtUser = txtUser.Text;
                _txtMoneda = txtMoneda.Text;
                _txtPrincipal = txtPrincipal.Text;
                _txtCustomer = txtCustomer.Text;
                _txtSucursal = txtSucursal.Text;
                _txtArtP = txtArtP.Text;
                _txtArtS = txtArtS.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido una inconsistencia al modificar el archivo de configuración \n " +
                                "Por favor contacte su administrador de sistema\n \n" + ex.Message,
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private bool ValidDataBases()
        {
            E_Resp resp = new E_Resp();

            resp = DataConection.Exist("pSeleccionarTransporte", 1, "@sCo_Tran", txtTransporte.Text);
            if(!resp.Status)
            {
                errorProvider1.SetError(txtTransporte, "Codigo transporte no encontrado");
                txtTransporte.Focus();
                return false;
            }
            errorProvider1.SetError(txtTransporte, "");

            resp = DataConection.Exist("pSeleccionarAlmacen", 1, "@sCo_Alma", txtAlmacen.Text);
            if (!resp.Status)
            {
                errorProvider1.SetError(txtAlmacen, "Codigo almacén no encontrado");
                txtAlmacen.Focus();
                return false;
            }
            errorProvider1.SetError(txtAlmacen, "");

            resp = DataConection.Exist("pSeleccionarTipoPrecio", 1, "@sCo_Precio", txtTipoPrecio.Text);
            if (!resp.Status)
            {
                errorProvider1.SetError(txtTipoPrecio, "Codigo tipo de precio no encontrado");
                txtTipoPrecio.Focus();
                return false;
            }
            errorProvider1.SetError(txtTipoPrecio, "");

            resp = DataConection.ExistIVA(txtIva.Text);
            if (!resp.Status)
            {
                errorProvider1.SetError(txtIva, "Codigo IVA no encontrado");
                txtIva.Focus();
                return false;
            }
            errorProvider1.SetError(txtIva, "");

            resp = DataConection.Exist("pSeleccionarUsuario", 0, "@sCod_Usuario", txtUser.Text);
            if (!resp.Status)
            {
                errorProvider1.SetError(txtUser, "Codigo usuario no encontrado");
                txtUser.Focus();
                return false;
            }
            errorProvider1.SetError(txtUser, "");

            resp = DataConection.Exist("pSeleccionarSucursal", 1, "@sCo_Sucur", txtPrincipal.Text);
            if (!resp.Status)
            {
                errorProvider1.SetError(txtPrincipal, "Codigo de Principal no encontrado");
                txtPrincipal.Focus();
                return false;
            }
            errorProvider1.SetError(txtPrincipal, "");

            resp = DataConection.Exist("pSeleccionarMoneda", 1, "@sCo_Mone", txtMoneda.Text);
            if (!resp.Status)
            {
                errorProvider1.SetError(txtMoneda, "Codigo de moneda no encontrado");
                txtMoneda.Focus();
                return false;
            }
            errorProvider1.SetError(txtMoneda, "");

            resp = DataConection.Exist("pSeleccionarCliente", 1, "@sCo_Cli", txtCustomer.Text);
            if (!resp.Status)
            {
                errorProvider1.SetError(txtCustomer, "Codigo de cliente no encontrado");
                txtCustomer.Focus();
                return false;
            }
            errorProvider1.SetError(txtCustomer, "");

            resp = DataConection.Exist("pSeleccionarArticulo", 1, "@sCo_Art", txtArtP.Text);
            if (!resp.Status)
            {
                errorProvider1.SetError(txtArtP, "Codigo de artículo no encontrado");
                txtArtP.Focus();
                return false;
            }
            errorProvider1.SetError(txtArtP, "");

            resp = DataConection.Exist("pSeleccionarArticulo", 1, "@sCo_Art", txtArtS.Text);
            if (!resp.Status)
            {
                errorProvider1.SetError(txtArtS, "Codigo de artículo no encontrado");
                txtArtS.Focus();
                return false;
            }
            errorProvider1.SetError(txtArtS, "");

            return true;
        }

        private void updateXml()
        {
            var root = new XmlDocument();
            root.Load(xlmname);
            XmlNode node = root.SelectSingleNode("ProbiaParameters/Parameters");
            node["Transport"].InnerText = txtTransporte.Text;
            node["Warehouse"].InnerText = txtAlmacen.Text;
            node["PriceType"].InnerText = txtTipoPrecio.Text;
            node["IvaType"].InnerText = txtIva.Text;
            node["User"].InnerText = txtUser.Text;
            node["Moneda"].InnerText = txtMoneda.Text;
            node["Principal"].InnerText = txtPrincipal.Text;
            node["Customer"].InnerText = txtCustomer.Text;
            node["Sucursal"].InnerText = txtSucursal.Text;
            node["ArtP"].InnerText = txtArtP.Text;
            node["ArtS"].InnerText = txtArtS.Text;
            root.Save(xlmname);
        }

        private bool ValidFields()
        {
            if (txtTransporte.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtTransporte, "Debe ingresar un codigo para el transporte.");
                txtTransporte.Focus();
                return false;
            }
            errorProvider1.SetError(txtTransporte, "");

            if (txtAlmacen.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtAlmacen, "Debe ingresar un codigo para el almacén.");
                txtAlmacen.Focus();
                return false;
            }
            errorProvider1.SetError(txtAlmacen, "");

            if (txtTipoPrecio.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtTipoPrecio, "Debe ingresar un codigo para el tipo de precio.");
                txtTipoPrecio.Focus();
                return false;
            }
            errorProvider1.SetError(txtTipoPrecio, "");

            if (txtIva.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtIva, "Debe ingresar un codigo para el tipo iva.");
                txtIva.Focus();
                return false;
            }
            errorProvider1.SetError(txtIva, "");

            if (txtUser.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtUser, "Debe ingresar un codigo para el usuario para las transacciones.");
                txtUser.Focus();
                return false;
            }
            errorProvider1.SetError(txtUser, "");

            if (txtPrincipal.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtPrincipal, "Debe ingresar código para la empresa principal");
                txtPrincipal.Focus();
                return false;
            }
            errorProvider1.SetError(txtPrincipal, "");


            if (txtMoneda.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtMoneda, "Debe ingresar código para la moneda");
                txtMoneda.Focus();
                return false;
            }
            errorProvider1.SetError(txtMoneda, "");

            if (txtCustomer.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtCustomer, "Debe ingresar código para cliente");
                txtCustomer.Focus();
                return false;
            }
            errorProvider1.SetError(txtCustomer, "");

            if (txtSucursal.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtSucursal, "Debe ingresar código para sucursal");
                txtSucursal.Focus();
                return false;
            }
            errorProvider1.SetError(txtSucursal, "");

            if (txtArtP.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtArtP, "Debe ingresar código para artículo principal");
                txtArtP.Focus();
                return false;
            }
            errorProvider1.SetError(txtArtP, "");

            if (txtArtS.Text.TrimEnd() == string.Empty)
            {
                errorProvider1.SetError(txtArtS, "Debe ingresar código para artículo sucursal");
                txtArtS.Focus();
                return false;
            }
            errorProvider1.SetError(txtArtS, "");

            return true;
        }

        private void CreateFolder(string foldername)
        {
            try
            {
                if (!Directory.Exists(foldername))
                {
                    Directory.CreateDirectory(foldername);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo crear la carpeta: Correspondiente a los parametros \n " +
                                "Por favor contacte su administrador de sistema\n \n" + ex.Message,
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void CreateXml(string route, string nodoroot)
        {
            try
            {
                // Create XML + Principal Node
                doc = new XmlDocument();
                XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                XmlNode root = doc.DocumentElement;
                doc.InsertBefore(xmlDeclaration, root);
                XmlNode element1 = doc.CreateElement(nodoroot);
                doc.AppendChild(element1);
                doc.Save(route);

                // Create Node y subnode
                _values.Add(txtTransporte.Text.TrimEnd());
                _values.Add(txtAlmacen.Text.TrimEnd());
                _values.Add(txtTipoPrecio.Text.TrimEnd());
                _values.Add(txtIva.Text.TrimEnd());
                _values.Add(txtUser.Text.TrimEnd());
                _values.Add(txtMoneda.Text.TrimEnd());
                _values.Add(txtPrincipal.Text.TrimEnd());
                _values.Add(txtCustomer.Text.TrimEnd());
                _values.Add(txtSucursal.Text.TrimEnd());
                _values.Add(txtArtP.Text.TrimEnd());
                _values.Add(txtArtS.Text.TrimEnd());

                CreateNode(xlmname, "Parameters", _name, _values);
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo crear el archivo de parametros\n " +
                                "Por favor contacte su administrador de sistema\n \n" + ex.Message,
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void CreateNode(string xlmname, string nodoname, List<string> _name, List<string> _values)
        {
            try
            {
                // Create Node
                doc = new XmlDocument();
                doc.Load(xlmname);

                XmlNode xNode = CreateSubNode(nodoname, _name, _values);
                XmlNode nodoroot = doc.DocumentElement;
                nodoroot.InsertAfter(xNode, nodoroot.LastChild);
                doc.Save(xlmname);
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo crear el archivo nodo en los parámetros \n " +
                                "Por favor contacte su administrador de sistema\n \n" + ex.Message,
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private XmlNode CreateSubNode(string nodoname, List<string> _name, List<string> _values)
        {
            XmlNode nodename = doc.CreateElement(nodoname);
            int i = 0;
            foreach (var item in _name)
            {
                XmlElement xsubnode = doc.CreateElement(item.ToString());
                xsubnode.InnerText = _values[i].ToString();
                nodename.AppendChild(xsubnode);
                i++;
            }
            return nodename;
        }

        public void ReadXml(string xlmname)
        {
            doc = new XmlDocument();
            doc.Load(xlmname);
            XmlNodeList route = doc.SelectNodes("ProbiaParameters/Parameters");
            XmlNode subnode = route.Item(0);
            txtTransporte.Text = subnode.SelectSingleNode("Transport").InnerText;
            _txtTransporte = subnode.SelectSingleNode("Transport").InnerText;
            txtAlmacen.Text = subnode.SelectSingleNode("Warehouse").InnerText;
            _txtAlmacen = subnode.SelectSingleNode("Warehouse").InnerText;
            txtTipoPrecio.Text = subnode.SelectSingleNode("PriceType").InnerText;
            _txtTipoPrecio = subnode.SelectSingleNode("PriceType").InnerText;
            txtIva.Text = subnode.SelectSingleNode("IvaType").InnerText;
            _txtIva = subnode.SelectSingleNode("IvaType").InnerText;
            txtUser.Text = subnode.SelectSingleNode("User").InnerText;
            _txtUser = subnode.SelectSingleNode("User").InnerText;
            txtMoneda.Text = subnode.SelectSingleNode("Moneda").InnerText;
            _txtMoneda = subnode.SelectSingleNode("Moneda").InnerText;
            txtPrincipal.Text = subnode.SelectSingleNode("Principal").InnerText;
            _txtPrincipal = subnode.SelectSingleNode("Principal").InnerText;
            txtCustomer.Text = subnode.SelectSingleNode("Customer").InnerText;
            _txtCustomer = subnode.SelectSingleNode("Customer").InnerText;
            txtSucursal.Text = subnode.SelectSingleNode("Sucursal").InnerText;
            _txtSucursal = subnode.SelectSingleNode("Sucursal").InnerText;
            txtArtP.Text = subnode.SelectSingleNode("ArtP").InnerText;
            _txtArtP = subnode.SelectSingleNode("ArtP").InnerText;
            txtArtS.Text = subnode.SelectSingleNode("ArtS").InnerText;
            _txtArtS = subnode.SelectSingleNode("ArtS").InnerText;
        }

        private void butMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
            this.frmMain.ChangeImageFoot(picIcon.Image, "4");
            this.frmMain.backColorButton();
        }

        private void frmParameters_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_txtTransporte != txtTransporte.Text || _txtAlmacen != txtAlmacen.Text ||
               _txtTipoPrecio != txtTipoPrecio.Text || _txtIva != txtIva.Text ||
               _txtUser != txtUser.Text || _txtMoneda != txtMoneda.Text ||
               _txtPrincipal != txtPrincipal.Text || _txtCustomer != txtCustomer.Text || 
               _txtSucursal != txtSucursal.Text || _txtArtP != txtArtP.Text || _txtArtS != txtArtS.Text)
            {
                DialogResult rta = MessageBox.Show("¿Desea de abandonar cambios?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (rta == DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }
            this.frmMain.backColorButton();
        }

        private void frmParameters_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void frmParameters_MouseClick(object sender, MouseEventArgs e)
        {
            if (ActiveMdiChild == null)
            {
                this.BringToFront();
                this.frmMain.backColorButton();
                this.frmMain.frontColorButton(3);

            }
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            frmParameters_MouseClick(null, null);
        }

    }
}
