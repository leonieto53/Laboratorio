﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace WinUp
{
    public interface IFormMain
    {
        void ChangeImageFoot(Image image, string text);
        void backColorButton();
        void frontColorButton(int but);
    }
}
