﻿using Business_Up;
using Entity_Up;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Reflection;

namespace WinUp
{
    public partial class frmUpload   : Form
    {

        public frmUpload()
        {
            InitializeComponent();
        }

        public IFormMain frmMain { get; set; }

        private int posY = 0;
        private int posX = 0;

        private List<E_CotizacionHead> heads = new List<E_CotizacionHead>();
        private E_CotizacionHead head = new E_CotizacionHead();
        private List<E_DocumentoHead> documents = new List<E_DocumentoHead>();
        private E_DocumentoHead document = new E_DocumentoHead();
        private List<E_CotizacionDetail> details = new List<E_CotizacionDetail>();
        private E_CotizacionDetail detail = new E_CotizacionDetail();
        private string NumerosLotesFact = string.Empty;
        private string NumerosLotesControl = string.Empty;
        private decimal STotalLotesFact = 0;
        private decimal TotalLotesFact = 0;
        private decimal TotalIvaLotesFact = 0;
        private DataTable customerDT;
        private string codven;
        private string codcon;
        private int plazpag;
        private string pc;
        private Decimal tasaiva = 0;
        private E_Resp serieDT;
        private DataTable LSerie;
        private bool IsProcesada = false;


        private string xlmname = Application.StartupPath + @"\Probia.Config\Probia.Parameters.xml";

        private string txtTransporte;
        private string txtAlmacen;
        private string txtTipoPrecio;
        private string txtIva;
        private string txtDelimiter;
        private string txtUser;
        private string txtMoneda;
        private string txtPrincipal;
        private string txtEnterprice;
        private string txtEnterprice2;
        private string txtCustomer;
        private string txtSucursal;
        private string txtArtP;
        private string txtArtS;

        private List<E_Customer> customers = new List<E_Customer>();

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            MovePanel(e);
        }

        private void MovePanel(MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                posX = e.X;
                posY = e.Y;
            }
            else
            {
                Left = Left + (e.X - posX);
                Top = Top + (e.Y - posY);
            }
        }

        private void frmSetup_Load(object sender, EventArgs e)
        {
            
            E_XmlFields field = DataConection.ReadXml();
            if(field.ok)
            {
                txtTransporte = field.p_transport;
                txtAlmacen = field.p_warehouse;
                txtTipoPrecio = field.p_priceType;
                txtIva = field.p_ivatype;
                txtDelimiter = field.p_delimiter;
                txtUser = field.p_user;
                txtMoneda = field.p_money;
                txtPrincipal = field.p_principal;
                txtEnterprice = field.c_database;
                txtEnterprice2 = field.c_database2;
                txtCustomer = field.p_customer;
                txtSucursal = field.p_sucursal;
                txtArtP = field.p_artp;
                txtArtS = field.p_arts;

                //butEncabezado.Enabled = false;
                //butSave.Enabled = false;
            }
            else
            {
                MessageBox.Show("Error al buscar datos de los parámetroa \n " + field.message,
                     "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            butEncabezado.Visible = false;
            butDeteleRecords.Visible = false;
            radioAll.Checked = true;
            hideColumns(false);
            butFind.Focus();
        }

        private void hideColumns(bool s)
        {
            if (dgvData.Rows.Count > 0)
            {
                dgvData.Columns[0].ReadOnly = true;
                dgvData.Columns[5].ReadOnly = true;
                dgvData.Columns[6].ReadOnly = true;
                dgvData.Columns[8].ReadOnly = true;
                dgvData.Columns[10].ReadOnly = true;
                dgvData.Columns[11].ReadOnly = true;
                dgvData.Columns[12].ReadOnly = true;
                dgvData.Columns[13].ReadOnly = true;
                dgvData.Columns[14].ReadOnly = true;
                dgvData.Columns[15].ReadOnly = true;
                dgvData.Columns[16].ReadOnly = true;
                dgvData.Columns[17].ReadOnly = true;
                dgvData.Columns[18].ReadOnly = true;

                dgvData.Columns[0].DefaultCellStyle.Format = "d";
                dgvData.Columns[8].DefaultCellStyle.Format = "##,##0.00";

                dgvData.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvData.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvData.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvData.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvData.Columns[0].Width = 80;
                dgvData.Columns[1].Width = 48; // Serie
                dgvData.Columns[2].Width = 90; // Nro Control
                dgvData.Columns[3].Width = 90; // Nro Factura
                dgvData.Columns[4].Width = 105; // Rif
                dgvData.Columns[5].Width = 60; // Anulada
                dgvData.Columns[7].Width = 262; // Nombre Cliente
                dgvData.Columns[8].Width = 130; // Total fact
                dgvData.Columns[9].Width = 50; // Sucursal
                dgvData.Columns[10].Width = 55; // Convenio
                dgvData.Columns[16].Width = 54; // Procesada

                dgvData.Columns[6].Visible = s;
                dgvData.Columns[11].Visible = s;
                dgvData.Columns[12].Visible = s;
                dgvData.Columns[13].Visible = s;
                dgvData.Columns[14].Visible = s;
                dgvData.Columns[15].Visible = s;
                //dgvData.Columns[16].Visible = s;
                dgvData.Columns[17].Visible = s;
                dgvData.Columns[18].Visible = s;

                dgvData.Focus();
            }
        }

        private void butExit_Click(object sender, EventArgs e)
        {
            frmMain form = new frmMain();
            form.SetbutSetup = Color.FromArgb(221, 234, 239);
            
            this.Close();
        }

        private void butSave_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (!ValidarDGV()) return;
            bool subs = true;
            if (!BreakFact()) return;

            if(IsProcesada)
            {
                DialogResult rta = MessageBox.Show("Una o mas facturas ya fueron procesadas en Profit, ¿Seguro de procesar facturas?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (rta == DialogResult.No)
                    return;
            }

            //Graba facturas en profit
            E_Resp resp = DataConection.InsertCotizacionHeadAll(heads, txtMoneda, details, subs, documents);
            if (!resp.Status)
            {
                MessageBox.Show("No se pudo guardar la información. \n" + resp.errorMessage,
                     "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                Cursor.Current = Cursors.Default;
                return;
            }

            MessageBox.Show("Información guardada en Profit satisfactoriamente.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

            // Actualizar campo procesado profit a verdadero
            StatusChange();

            butClean_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void StatusChange()
        {
            foreach(DataGridViewRow row in dgvData.Rows)
            {
                Int32 NumFact = Convert.ToInt32(row.Cells[18].Value.ToString().Trim());

                List<SqlParameter> prm = new List<SqlParameter>()
                {
                    new SqlParameter("@sNfact", NumFact),
                    new SqlParameter("@xValor", "1"),
                    new SqlParameter("@xField", 10 )
                };
                E_Resp updateDT = DataConection.ActualizarTabla("pModificaFacturas", 3, prm);
                if (!updateDT.Status)
                    MessageBox.Show("No se pudo cambiar el estatus de procesada de las facturas en IBetel\n" + updateDT.errorMessage,
                                             "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
            }
        }

        private void DeleteRecords()
        {
            // Elimina los registros de IBetel correspondiente al rango de fechas
            List<SqlParameter> prm = new List<SqlParameter>()
                {
                    new SqlParameter("@factura_impresa", System.Data.SqlTypes.SqlString.Null),
                    new SqlParameter("@all", "ALL"),
                    new SqlParameter("@dfecha_d", dtpDesde.Value),
                    new SqlParameter("@dfecha_h", dtpHasta.Value),
                };
            E_Resp facDT = DataConection.DeleteRecord("pEliminarRegistro", 3, prm);
            if (!facDT.Status)
            {
                MessageBox.Show("No se pudo eliminar los registros de IBETEL \n" + facDT.errorMessage,
                     "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                Cursor.Current = Cursors.Default;
                return;
            }
        }

        private bool ValidFields()
        {
            if (dgvData.Rows.Count == 0)
            {
                errorProvider1.SetError(dgvData, "No hay registros para el proceso.");
                butFind.Focus();
                return false;
            }
            errorProvider1.SetError(dgvData, "");

            return true;
        }

        private void butTest_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (dgvData.Rows.Count == 0)
            {
                MessageBox.Show("No hay registros a procesar", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                return;
            }

            if(ValidarDGV())
            {
                MessageBox.Show("Validación correcta!", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                BreakFact();
                butAlgoritmo.Visible = true;
            }

            Cursor.Current = Cursors.Default;
        }

        private bool ValidarDGV()
        {
            butAlgoritmo.Visible = false;
            butEncabezado.Visible = false;

            string mess = "Se encontraron una ó mas inconsistencias: ";
            bool IsEmpty = false;
            bool IsCero = false;
            bool IsCustomer = false;
            bool IsSucursal = false;
            bool IsSerie = false;
            IsProcesada = false;
            bool IsAnulada = false;
            bool IsFecha = false;
            bool IsOk = true;

            customers.Clear();

            foreach (DataGridViewRow row in dgvData.Rows)
            {
                if (row.DefaultCellStyle.BackColor != Color.White || row.DefaultCellStyle.BackColor != Color.WhiteSmoke)
                    ColorFila(row);

                int lin = dgvData.Rows.IndexOf(row);

                // Cualquier celda vacia
                foreach (DataGridViewCell dc in row.Cells)
                {
                    if (dc.ColumnIndex == 0 || dc.ColumnIndex == 1 || dc.ColumnIndex == 2
                        || dc.ColumnIndex == 3 || dc.ColumnIndex == 4 || dc.ColumnIndex == 7
                        || dc.ColumnIndex == 9 || dc.ColumnIndex == 10 || dc.ColumnIndex == 12)
                    {
                        if (dc.Value == null || string.IsNullOrEmpty(dc.Value.ToString()))
                        {
                            IsEmpty = true;
                            IsOk = false;
                            row.DefaultCellStyle.BackColor = Color.FromArgb(255, 223, 130);
                        }
                    }
                    if (dc.ColumnIndex == 8 && (dc.Value == null || Convert.ToDecimal(dc.Value.ToString()) == 0))
                    {
                        IsCero = true;
                        IsOk = false;
                        row.DefaultCellStyle.BackColor = Color.FromArgb(255, 223, 130);
                    }
                }


                // Valida Nro. serie
                string codserie = row.Cells[1].Value.ToString();
                serieDT = DataConection.ConsultaIBetel("pSeleccionarSerie", 3);

                if (!serieDT.Status)
                {
                    MessageBox.Show("No se encontraron registros en la tabla: Series",
                         "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    IsSerie = true;
                    IsOk = false;
                    row.DefaultCellStyle.BackColor = Color.FromArgb(189, 183, 107);
                }
                LSerie = (DataTable)serieDT.Obj;

                bool contains = LSerie.AsEnumerable().Any(s => codserie.PadRight(3) == s.Field<String>("Codigo_Serie"));
                if (!contains)
                {
                    IsSerie = true;
                    IsOk = false;
                    row.DefaultCellStyle.BackColor = Color.FromArgb(189, 183, 107);
                }

                // Valida existencia de clientes en Profit 
                string rif = row.Cells[4].Value.ToString();
                string cli = row.Cells[7].Value.ToString();
                string fila = row.Index.ToString(); 
                string convenio = row.Cells[10].Value.ToString().Trim();

                //if (string.IsNullOrEmpty(rif) || convenio != "1")

                if (rif.Trim().Length < 2 || string.IsNullOrEmpty(rif) || (!Char.IsNumber(rif, 1) && 
                    (rif.Substring(0, 1).ToUpper() != "V") && rif.Substring(0, 1).ToUpper() != "E" ))
                {
                    string query = "select * from saCliente where (REPLACE(rif,'-','') = REPLACE('" + rif + "','-',''))";
                    E_Resp cus = DataConection.ConsultarQuery(query, 1);
                    if(!cus.Status)
                    {
                        IsCustomer = true;
                        IsOk = false;

                        E_Customer Customer = new E_Customer();
                        Customer.Rif = rif;
                        Customer.Nombre = cli;
                        Customer.fila = fila;

                        if (!customers.Contains(Customer))
                        {
                            customers.Add(Customer);
                        }
                        row.DefaultCellStyle.BackColor = Color.Salmon;
                    }
                }

                // Valida Sucursal
                string suc = row.Cells[9].Value.ToString().Trim();
                if (suc != txtPrincipal && suc != txtSucursal)
                {
                    IsSucursal = true;
                    IsOk = false;
                    row.DefaultCellStyle.BackColor = Color.Khaki;
                }

                // Valida si fue procesada...
                bool _procesada = Convert.ToBoolean(row.Cells[16].Value.ToString());
                if (_procesada == true)
                {
                    IsProcesada = true;
                    //IsOk = false;
                    //row.DefaultCellStyle.BackColor = Color.Aqua;
                }
            }

            if (IsEmpty)
                mess = mess + " | Columas vacias";
            if (IsCero)
                mess = mess + " | Columas total en cero (0.00)";
            if (IsSerie)
                mess = mess + " | Número de Serie no encontrado";
            if (IsCustomer)
            {
                mess = mess + " | Clientes no creados en Profit";
                butEncabezado.Visible = true;
            }
            if (IsSucursal)
                mess = mess + " | Sucursal no definida";

            if (mess != "Se encontraron una ó mas inconsistencias: ")
            {
                MessageBox.Show(mess, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
            }

            string metodo = GetCallerName();

            if (IsProcesada && metodo == "butTest_Click")
            {
                MessageBox.Show("Una o mas facturas ya fueron procesadas en profit", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
            }

            return IsOk;
        }

        // Metodo que devuelve el nombre de la funcion del que fue llamado
        private static string GetCallerName()
        {
            StackTrace trace = new StackTrace(StackTrace.METHODS_TO_SKIP + 2);
            StackFrame frame = trace.GetFrame(0);
            MethodBase caller = frame.GetMethod();
            if (caller == null)
            {
                throw new InvalidProgramException();
            }
            return caller.Name;
        }


        private void ColorFila(DataGridViewRow row)
        {
            if ((row.Index % 2) == 0)
                row.DefaultCellStyle.BackColor = Color.White;
            else
                row.DefaultCellStyle.BackColor = Color.WhiteSmoke;
        }


        //****************************************************************
        // Algoritmo que construye la facturas hacia profit
        private bool BreakFact()
        {
            E_Resp resp = new E_Resp();
            heads = new List<E_CotizacionHead>();
            documents = new List<E_DocumentoHead>();
            details = new List<E_CotizacionDetail>();
            NumerosLotesFact = string.Empty;
            NumerosLotesControl = string.Empty;
            STotalLotesFact = 0;
            TotalLotesFact = 0;
            TotalIvaLotesFact = 0;

            try
            {
                dgvData.CurrentCell = dgvData.Rows[0].Cells[0];
                // Buscar cliente y traer otros datos datos
                plazpag = 0;
                E_Resp cliDT = DataConection.Exist("pSeleccionarCliente", 1, "@sCo_Cli", txtCustomer);
                if (!cliDT.Status)
                {
                    MessageBox.Show("Codigo de cliente no encontrado.",
                         "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                    return false;
                }
                // Trae OBJ anonimo de cliente generico
                customerDT = (DataTable)cliDT.Obj;
                codven = customerDT.Rows[0]["co_ven"].ToString();
                codcon = customerDT.Rows[0]["cond_pag"].ToString();
                plazpag = Convert.ToInt32(customerDT.Rows[0]["plaz_pag"].ToString());

                // Busca porcentaje tasa IVA unico "porc_tasa"
                E_Resp ivaDT = DataConection.ExistIVA(txtIva);
                DataTable IvaRow = (DataTable)ivaDT.Obj;
                tasaiva = Convert.ToDecimal(IvaRow.Rows[0]["porc_tasa"].ToString());

                // Nombre PC
                pc = Environment.MachineName;

                // Trae series
                serieDT = DataConection.ConsultaIBetel("pSeleccionarSerie", 3);
                LSerie = (DataTable)serieDT.Obj;

                // busca unidad del producto
                E_Resp uniPDT = DataConection.Exist("pSeleccionarUnidadArticulo", 1, "@sCo_Art", txtArtP);
                E_Resp uniSDT = DataConection.Exist("pSeleccionarUnidadArticulo", 1, "@sCo_Art", txtArtS);
                DataTable uniP = (DataTable)uniPDT.Obj;
                DataTable uniS = (DataTable)uniSDT.Obj;
                DataRow[] unitP = uniP.Select("uni_principal = true");
                DataRow[] unitS = uniS.Select("uni_principal = true");
                string undP = unitP[0]["co_uni"].ToString();
                string undS = unitP[0]["co_uni"].ToString();

                // Variables para el recorrido y validacion
                DateTime fecha_a = Convert.ToDateTime(dgvData.Rows[dgvData.CurrentRow.Index].Cells[0].Value.ToString());
                string serie_a = dgvData.Rows[dgvData.CurrentRow.Index].Cells[1].Value.ToString();
                string facturai_a = dgvData.Rows[dgvData.CurrentRow.Index].Cells[3].Value.ToString();
                string rif_a = dgvData.Rows[dgvData.CurrentRow.Index].Cells[4].Value.ToString();
                string anulada_a = dgvData.Rows[dgvData.CurrentRow.Index].Cells[5].Value.ToString().Trim().ToUpper();
                string sucursal_a = dgvData.Rows[dgvData.CurrentRow.Index].Cells[9].Value.ToString();
                string unid_a = (txtPrincipal.Trim() == sucursal_a.Trim() ? undP : undS);
                string convenio_a = dgvData.Rows[dgvData.CurrentRow.Index].Cells[10].Value.ToString();

                string current_rif = string.Empty;
                string current_serie = string.Empty;
                string current_anulada = string.Empty;
                string current_sucursal = string.Empty;
                string current_unid = string.Empty;
                string current_facturai = string.Empty;
                string current_convenio = string.Empty;
                DateTime current_fecha = DateTime.Now;
                

                // ************************** Ciclo datagrid *************************** //
                foreach (DataGridViewRow row in dgvData.Rows)
                {
                    
                    // Condicion para el rompimiento....
                    current_rif = dgvData.Rows[row.Index].Cells[4].Value.ToString();
                    current_serie = dgvData.Rows[row.Index].Cells[1].Value.ToString();
                    current_facturai = dgvData.Rows[row.Index].Cells[3].Value.ToString();
                    current_anulada = dgvData.Rows[row.Index].Cells[5].Value.ToString().Trim().ToUpper();
                    current_fecha = Convert.ToDateTime(dgvData.Rows[row.Index].Cells[0].Value.ToString()).Date;
                    current_sucursal = dgvData.Rows[row.Index].Cells[9].Value.ToString().Trim();
                    current_convenio = dgvData.Rows[row.Index].Cells[10].Value.ToString().Trim();
                    current_unid = (txtPrincipal.Trim() == current_sucursal.Trim() ? undP : undS);
                    // otras columnas
                    string nControl = dgvData.Rows[row.Index].Cells[2].Value.ToString();

                    // Break to date
                    if (current_fecha != fecha_a.Date)
                    {
                        if (anulada_a != "A")
                        {
                            AddList(fecha_a, anulada_a, serie_a, rif_a, NumerosLotesControl, NumerosLotesFact, STotalLotesFact, TotalIvaLotesFact, TotalLotesFact, sucursal_a, unid_a, facturai_a, convenio_a);
                            CleanVar();
                        }
                    }
                    // break to Serie .... Incluye romper por sucursal
                    else if (current_serie != serie_a)
                    {
                        if (anulada_a != "A")
                        {
                            AddList(fecha_a, anulada_a, serie_a, rif_a, NumerosLotesControl, NumerosLotesFact, STotalLotesFact, TotalIvaLotesFact, TotalLotesFact, sucursal_a, unid_a, facturai_a, convenio_a);
                            CleanVar();
                        }
                        // SI cambia de serie y ademas es anulada debe romper
                        if (current_anulada == "A")
                        {
                            NumerosLotesControl = NumerosLotesControl + dgvData.Rows[row.Index].Cells[2].Value.ToString().Trim() + ", ";
                            NumerosLotesFact = NumerosLotesFact + dgvData.Rows[row.Index].Cells[3].Value.ToString().Trim() + ", ";
                            STotalLotesFact = STotalLotesFact + Convert.ToDecimal(dgvData.Rows[row.Index].Cells[12].Value.ToString());
                            TotalIvaLotesFact = TotalIvaLotesFact + Convert.ToDecimal(dgvData.Rows[row.Index].Cells[14].Value.ToString());
                            TotalLotesFact = TotalLotesFact + Convert.ToDecimal(dgvData.Rows[row.Index].Cells[8].Value.ToString());
                            AddList(current_fecha, current_anulada, current_serie, current_rif, NumerosLotesControl, NumerosLotesFact, STotalLotesFact, TotalIvaLotesFact, TotalLotesFact, current_sucursal, current_unid, current_facturai, current_convenio);
                            CleanVar();
                        }

                    }
                    // break to Anulada
                    else if (current_anulada == "A")
                    {
                        if (anulada_a != "A")
                        {
                            AddList(fecha_a, anulada_a, serie_a, rif_a, NumerosLotesControl, NumerosLotesFact, STotalLotesFact, TotalIvaLotesFact, TotalLotesFact, sucursal_a, unid_a, facturai_a, convenio_a);
                            CleanVar();
                        }
                        NumerosLotesControl = NumerosLotesControl + dgvData.Rows[row.Index].Cells[2].Value.ToString().Trim() + ", ";
                        NumerosLotesFact = NumerosLotesFact + dgvData.Rows[row.Index].Cells[3].Value.ToString().Trim() + ", ";
                        STotalLotesFact = STotalLotesFact + Convert.ToDecimal(dgvData.Rows[row.Index].Cells[12].Value.ToString());
                        TotalIvaLotesFact = TotalIvaLotesFact + Convert.ToDecimal(dgvData.Rows[row.Index].Cells[14].Value.ToString());
                        TotalLotesFact = TotalLotesFact + Convert.ToDecimal(dgvData.Rows[row.Index].Cells[8].Value.ToString());
                        AddList(current_fecha, current_anulada, current_serie, current_rif, NumerosLotesControl, NumerosLotesFact, STotalLotesFact, TotalIvaLotesFact, TotalLotesFact, current_sucursal, current_unid, current_facturai, current_convenio);
                        CleanVar();
                    }
                    // Break to Rif
                    else if (!IsRif(current_rif, rif_a)) // si no es "V"
                    {
                        if (anulada_a != "A")
                        {
                            if (current_rif != rif_a)
                            {
                                AddList(fecha_a, anulada_a, serie_a, rif_a, NumerosLotesControl, NumerosLotesFact, STotalLotesFact, TotalIvaLotesFact, TotalLotesFact, sucursal_a, unid_a, facturai_a, convenio_a);
                                CleanVar();
                            }
                        }
                    }

                    if (current_anulada != "A")
                    {
                        NumerosLotesControl = NumerosLotesControl + dgvData.Rows[row.Index].Cells[2].Value.ToString().Trim() + ", ";
                        NumerosLotesFact = NumerosLotesFact + dgvData.Rows[row.Index].Cells[3].Value.ToString().Trim() + ", ";
                        STotalLotesFact = STotalLotesFact + Convert.ToDecimal(dgvData.Rows[row.Index].Cells[12].Value.ToString());
                        TotalIvaLotesFact = TotalIvaLotesFact + Convert.ToDecimal(dgvData.Rows[row.Index].Cells[14].Value.ToString());
                        TotalLotesFact = TotalLotesFact + Convert.ToDecimal(dgvData.Rows[row.Index].Cells[8].Value.ToString());
                    }
                    fecha_a = Convert.ToDateTime(dgvData.Rows[row.Index].Cells[0].Value.ToString());
                    serie_a = dgvData.Rows[row.Index].Cells[1].Value.ToString();
                    facturai_a = dgvData.Rows[row.Index].Cells[3].Value.ToString();
                    anulada_a = dgvData.Rows[row.Index].Cells[5].Value.ToString().Trim().ToUpper();
                    rif_a = dgvData.Rows[row.Index].Cells[4].Value.ToString();
                    sucursal_a = dgvData.Rows[row.Index].Cells[9].Value.ToString();
                    convenio_a = dgvData.Rows[row.Index].Cells[10].Value.ToString();
                    unid_a = (txtPrincipal.Trim() == sucursal_a.Trim() ? undP : undS);

                    // Ultimo registro
                    if (row.Index == dgvData.Rows.Count-1)
                    {
                        if (anulada_a != "A")
                        {
                            AddList(fecha_a, anulada_a, serie_a, rif_a, NumerosLotesControl, NumerosLotesFact, STotalLotesFact, TotalIvaLotesFact, TotalLotesFact, sucursal_a, unid_a, facturai_a, convenio_a);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al procesar datos en algoritmo \n " + ex.Message,
                 "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return false;
            }

            return true;
        }

        private void CleanVar()
        {
            NumerosLotesControl = string.Empty;
            NumerosLotesFact = string.Empty;
            STotalLotesFact = 0;
            TotalIvaLotesFact = 0;
            TotalLotesFact = 0;
        }


        // Agrega registro a los objetos: encabezado fact., detalle fact. y documento.
        private void AddList(DateTime fecha_a, string anulada_a, string serie_a, string rif_a,
            string nControl, string nFacturas, decimal STotalLotesFact, decimal TotalIvaLotesFact, 
            decimal TotalLotesFact, string sucursal, string unid, string facturai, string convenio)
        {
            head = new E_CotizacionHead();
            detail = new E_CotizacionDetail();
            document = new E_DocumentoHead();

            DataTable customerDT2 = null;
            string codcli2 = string.Empty;
            string codven2 = string.Empty;
            string codcon2 = string.Empty;
            Int32 plazpag2 = 0;

            string[] subcad_F = nFacturas.Split(',');
            string[] subcad_C = nControl.Split(',');

            // Se asigana codigo cliente  variables 2
            codcli2 = txtCustomer;
            codven2 = codven;
            codcon2 = codcon;
            plazpag2 = plazpag;

            // Si Es diferente a natural busca cliente y asigna codigo de cliente de profit
            if (subcad_F.Length == 1 && 
                (rif_a.Substring(0, 1).ToUpper() != "V" && rif_a.Substring(0, 1).ToUpper() != "E" && !Char.IsNumber(rif_a, 1)))
            {
                string query = "select * from saCliente where (REPLACE(rif,'-','') = REPLACE('" + rif_a + "','-',''))";
                E_Resp cus = DataConection.ConsultarQuery(query, 1);
                // Trae OBJ anonimo de cliente
                if (!cus.Status)
                {
                    customerDT2 = (DataTable)cus.Obj;
                    codcli2 = customerDT.Rows[0]["co_cli"].ToString();
                    codven2 = customerDT.Rows[0]["co_ven"].ToString();
                    codcon2 = customerDT.Rows[0]["cond_pag"].ToString();
                    plazpag2 = Convert.ToInt32(customerDT2.Rows[0]["plaz_pag"].ToString());
                }
            }

            // Encuentra descripcion de serie
            string serie_descrip  = (from a in LSerie.AsEnumerable() where a.Field<string>("Codigo_Serie") == serie_a.PadRight(3) select a.Field<string>("Descripcion")).SingleOrDefault();

            // Llena Objeto Head
            head.sDescrip = facturai;
            head.sCo_Cli = codcli2;
            head.sCo_Tran = txtTransporte;
            head.sCo_Mone = txtMoneda;
            head.sCo_Ven = codven2;
            head.sCo_Cond = codcon2;
            head.sdFec_Emis = Convert.ToDateTime(fecha_a, CultureInfo.InvariantCulture);
            head.sdFec_Venc = head.sdFec_Emis.AddDays(plazpag2);
            head.sdFec_Reg = head.sdFec_Emis;
            head.bAnulado = (anulada_a.Trim().ToUpper() == "A") ? true : false;
            head.sStatus = "0";
            head.deTasa = 1;
            head.deMonto_Desc_Glob = 0;
            head.deMonto_Reca = 0;
            head.deTotal_Bruto = STotalLotesFact;
            head.deMonto_Imp = TotalIvaLotesFact;
            head.deTotal_Neto = head.deTotal_Bruto + head.deMonto_Imp;
            head.deSaldo = head.deTotal_Neto;
            head.deMonto_Imp2 = 0;
            head.deMonto_Imp3 = 0;
            head.deOtros1 = 0;
            head.deOtros2 = 0;
            head.deOtros3 = 0;
            head.bContrib = false;
            head.bImpresa = false;
            head.bVen_Ter = false;
            head.sCo_Us_In = txtUser;
            head.sMaquina = pc;
            head.sdFec_Emis = fecha_a;
            head.sdFec_Reg = fecha_a;
            head.sCampo1 = serie_descrip;
            head.sCampo2 = subcad_F[0] + (subcad_F.Length > 2 ? "/" + subcad_F[subcad_F.Length - 2] : "");
            head.sCampo3 = subcad_C[0] + (subcad_C.Length > 2 ? "/" + subcad_C[subcad_C.Length - 2] : "");
            head.sCampo4 = anulada_a;
            head.sCampo5 = serie_a;
            head.sCampo6 = (subcad_F.Length > 2 ? (!IsRif(rif_a, rif_a) ? rif_a : "VARIOS") : (anulada_a == "A" ? "XXXX" : rif_a)); 
            head.deTotal_Bruto = STotalLotesFact;
            head.deMonto_Imp = TotalIvaLotesFact;
            head.deTotal_Neto = TotalLotesFact;
            // Add record to array
            heads.Add(head);

            // Llena objeto document
            document.sCampo1 = facturai;
            document.sCo_Tipo_Doc = "FACT";
            document.sCo_Cli = codcli2;
            document.sCo_Ven = codven2;
            document.sCo_Mone = txtMoneda;
            document.deTasa = 1;
            document.sObserva = "FACT No. de Cliente " + txtCustomer;
            document.sdFec_Emis = Convert.ToDateTime(fecha_a, CultureInfo.InvariantCulture);
            document.sdFec_Venc = head.sdFec_Emis.AddDays(plazpag2);
            document.sdFec_Reg = head.sdFec_Emis;
            document.bAnulado = (anulada_a.Trim().ToUpper() == "A") ? true : false;
            document.bAut = true;
            document.bContrib = false;
            document.sDoc_Orig = "FACT";
            document.iTipo_Origen = 0;
            document.deSaldo = head.deTotal_Neto;
            document.deTotal_Bruto = head.deTotal_Bruto;
            document.deMonto_Desc_Glob = 0;
            document.deMonto_Reca = 0;
            document.deTotal_Neto = head.deTotal_Neto;
            document.deMonto_Imp = 0;
            document.deMonto_Imp2 = 0;
            document.deMonto_Imp3 = 0;
            document.sTipo_Imp = "1";
            document.dePorc_Imp = 0;
            document.dePorc_Imp2 = 0;
            document.dePorc_Imp3 = 0;
            document.sN_Control = subcad_C[0] + (subcad_C.Length > 2 ? "/" + subcad_C[subcad_C.Length - 2] : "");
            document.deComis1 = 0;
            document.deComis2 = 0;
            document.deComis3 = 0;
            document.deComis4 = 0;
            document.deComis5 = 0;
            document.deComis6 = 0;
            document.deAdicional = 0;
            document.bVen_Ter = false;
            document.deOtros1 = 0;
            document.deOtros2 = 0;
            document.deOtros3 = 0;
            document.sCo_Us_In = txtUser;
            // Add record to array
            documents.Add(document);

            // Llena objeto detail
            detail.sCo_Art = (txtPrincipal.Trim() == sucursal.Trim() ? txtArtP : txtArtS);
            detail.sCo_Alma = txtAlmacen;
            detail.sCo_Uni = unid;
            detail.sCo_Precio = txtTipoPrecio;
            detail.sTipo_Imp = txtIva;
            detail.dePorc_Imp = tasaiva;
            detail.dePorc_Imp2 = 0;
            detail.dePorc_Imp3 = 0;
            detail.sComentario = facturai;
            detail.iReng_Num = 1;
            detail.deTotal_Art = 1;
            detail.dePendiente = detail.deTotal_Art;
            detail.deSTotal_Art = 0;
            detail.dePrec_Vta = Convert.ToDecimal(STotalLotesFact, CultureInfo.InvariantCulture);
            detail.deMonto_Desc = 0;
            detail.deReng_Neto = Convert.ToDecimal(TotalLotesFact, CultureInfo.InvariantCulture);
            detail.deMonto_Desc_Glob = 0;
            detail.deMonto_reca_Glob = 0;
            detail.deOtros1_glob = 0;
            detail.deOtros2_glob = 0;
            detail.deOtros3_glob = 0;
            detail.deMonto_imp_afec_glob = 0;
            detail.deMonto_imp2_afec_glob = 0;
            detail.deMonto_imp3_afec_glob = 0;
            detail.deMonto_Imp = Convert.ToDecimal(TotalIvaLotesFact, CultureInfo.InvariantCulture);
            detail.deMonto_Imp2 = 0;
            detail.deMonto_Imp3 = 0;
            detail.deOtros = 0;
            detail.deTotal_Dev = 0;
            detail.deMonto_Dev = 0;
            detail.sCo_Sucu_In = txtSucursal;
            detail.sCo_Us_In = txtUser;
            detail.sMaquina = pc;
            // Add record to array
            details.Add(detail);
        }

        private bool IsRif(string currenRif, string rifA)
        {
            bool resp = true;

            //if(Char.IsNumber(currenRif, 1))
            //{
            //    resp = true;
            //}
            if (currenRif.Substring(0, 1).ToUpper() != "V" && currenRif.Substring(0, 1).ToUpper() != "E" && !Char.IsNumber(currenRif, 1)) 
            {
                resp = false;
            }
            else if((rifA.Substring(0, 1).ToUpper() != "V" && rifA.Substring(0, 1).ToUpper() != "E") && !Char.IsNumber(rifA, 1))
            {
                resp = false;
            }

            return resp;
        }

        private void butEncabezado_Click(object sender, EventArgs e)
        {
            var cus = customers.GroupBy(x => x.Rif).Select(y => y.First()).ToList();
            
            frmCustomer form = new frmCustomer(cus);
            form.ShowDialog();
        }

        private void frmUpload_FormClosing(object sender, FormClosingEventArgs e)
        {
            //var ifsave = ListHead.Where(x => x.sDoc_Num != null).ToList();
            //if (ifsave.Count == 0 && dgvData.Rows.Count > 0)
            //{
            //    DialogResult rta = MessageBox.Show("¿Desea de abandonar cambios?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            //    if (rta == DialogResult.No)
            //    {
            //        e.Cancel = true;
            //        return;
            //    }
            //}
            this.frmMain.backColorButton();
        }

        private void butMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
            this.frmMain.ChangeImageFoot(picIcon.Image, "1");
            this.frmMain.backColorButton();
        }

        private void frmUpload_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void frmUpload_MouseClick(object sender, MouseEventArgs e)
        {
            if(ActiveMdiChild == null)
            {
                this.BringToFront();
                this.frmMain.backColorButton();
                this.frmMain.frontColorButton(1);
            }
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            frmUpload_MouseClick(null,null);
        }

        private void butFind_Click_1(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            dgvData.DataSource = null;
            butEncabezado.Visible = false;
            butAlgoritmo.Visible = false;
            customers.Clear();
            radioAll.Checked = true;
            checkMostrarColumnas.Checked = false;
            txtTotalRecords.Text = string.Empty;
            txtTotalFacturas.Text = string.Empty;
            IsProcesada = false;

            try
            {
                bool procesada = false;

                string sucursal = null;
                if(radioPpal.Checked)
                    { sucursal = txtPrincipal; }
                else if (radioSuc.Checked)
                    { sucursal = txtMoneda; }

                List<SqlParameter> prm = new List<SqlParameter>()
                {
                    new SqlParameter("@sProcesada", procesada),
                    new SqlParameter("@dfecha_d", dtpDesde.Value),
                    new SqlParameter("@dfecha_h", dtpHasta.Value),
                    new SqlParameter("@sucursal", sucursal)
                };

                E_Resp facDT = DataConection.ConsultaIBetel("pSeleccionarFacturas", 3, prm);
                if (!facDT.Status)
                {
                    dgvData.DataSource = null;
                    MessageBox.Show("No se encontraron facturas pendientes por procesar\n" + facDT.errorMessage,
                         "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                butEncabezado.Visible = false;
                butDeteleRecords.Visible = true;
                dgvData.DataSource = facDT.Obj;
                hideColumns(false);
                sumaDGV();
                dgvData.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo ejecutar la busqueda \n" + ex.Message
                    , "Advertencia!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                txtTotalFacturas.Text = string.Empty;
                txtTotalRecords.Text = string.Empty;
            }
            Cursor.Current = Cursors.Default;
        }

        private void sumaDGV()
        {
            txtTotalRecords.Text = string.Format("{0:#,##0}", double.Parse(dgvData.Rows.Count.ToString().ToString())); 
            decimal suma = 0;
            foreach (DataGridViewRow row in dgvData.Rows)
            {
                if (row.Cells["total_factura"].Value != null)
                    suma += (decimal)row.Cells["total_factura"].Value;
            }
            txtTotalFacturas.Text = string.Format("{0:#,##0.00}", double.Parse(suma.ToString()));
        }

        private void checkMostrarColumnas_CheckedChanged(object sender, EventArgs e)
        {
            hideColumns(checkMostrarColumnas.Checked);
        }

        private void dgvData_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridViewTextBoxEditingControl text = (DataGridViewTextBoxEditingControl)e.Control;
            text.KeyPress -= new KeyPressEventHandler(textbox_KeyPress);
            text.KeyPress += new KeyPressEventHandler(textbox_KeyPress);
        }

        private void textbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = char.ToUpper(e.KeyChar);
        }

        private void dgvData_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            ((DataGridViewTextBoxColumn)dgvData.Columns[1]).MaxInputLength = 2; // Serie
            ((DataGridViewTextBoxColumn)dgvData.Columns[2]).MaxInputLength = 15; // Control
            ((DataGridViewTextBoxColumn)dgvData.Columns[3]).MaxInputLength = 20; // Factura
            ((DataGridViewTextBoxColumn)dgvData.Columns[4]).MaxInputLength = 12; // Rif
            ((DataGridViewTextBoxColumn)dgvData.Columns[5]).MaxInputLength = 1; // Anulada
        }

        private void dgvData_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex > 0 && e.ColumnIndex <= 5) || e.ColumnIndex == 7 || e.ColumnIndex == 9)
            {
                string ValueCell = dgvData.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                Int32 ValueFact = Convert.ToInt32(dgvData.Rows[e.RowIndex].Cells[18].Value.ToString());

                List<SqlParameter> prm = new List<SqlParameter>()
                            {
                                new SqlParameter("@sNfact", ValueFact),
                                new SqlParameter("@xValor", ValueCell),
                                new SqlParameter("@xField", e.ColumnIndex )
                            };

                E_Resp updateDT = DataConection.ActualizarTabla("pModificaFacturas", 3, prm);
                if (!updateDT.Status)
                    MessageBox.Show("Error al actualizar los cambios \n" + updateDT.errorMessage,
                                             "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
            }
        }

        private void butClean_Click(object sender, EventArgs e)
        {
            dtpDesde.Value = DateTime.Now;
            dtpHasta.Value = DateTime.Now;
            dgvData.DataSource = null;
            butEncabezado.Visible = false;
            butAlgoritmo.Visible = false;
            butDeteleRecords.Visible = false;
            customers.Clear();
            radioAll.Checked = true;
            checkMostrarColumnas.Checked = false;
            txtTotalRecords.Text = string.Empty;
            txtTotalFacturas.Text = string.Empty;
            butFind.Enabled = true;
            IsProcesada = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmAddRecord frm = new frmAddRecord();
            frm.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Eliminar registro
            if (dgvData.Rows.Count == 0)
                return;

            string ValueCell = dgvData.Rows[dgvData.CurrentRow.Index].Cells["Nro_Factura"].Value.ToString();
            List<SqlParameter> prm = new List<SqlParameter>()
                {
                    new SqlParameter("@factura_impresa", ValueCell.Trim()),
                    new SqlParameter("@all", System.Data.SqlTypes.SqlString.Null),
                    new SqlParameter("@dfecha_d", System.Data.SqlTypes.SqlDateTime.Null),
                    new SqlParameter("@dfecha_h", System.Data.SqlTypes.SqlDateTime.Null),
                };

            DialogResult rta = MessageBox.Show("¿Seguro de eliminar factura?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (rta == DialogResult.No)
                return;

            E_Resp facDT = DataConection.DeleteRecord("pEliminarRegistro", 3, prm);
            if (!facDT.Status)
            {
                MessageBox.Show("No se pudo eliminar el registro \n" + facDT.errorMessage,
                     "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                Cursor.Current = Cursors.Default;
                return;
            }
            else
            {
                dgvData.Rows.RemoveAt(dgvData.CurrentRow.Index);
                MessageBox.Show("Registro eliminado exitosamente!", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
        }

        private void butAlgoritmo_Click(object sender, EventArgs e)
        {
            frmAlgoritmo form = new frmAlgoritmo(heads);
            form.ShowDialog();

        }

        private void butDeteleRecords_Click(object sender, EventArgs e)
        {
            DialogResult rta = MessageBox.Show("Este proceso borrará las facturas seleccionadas definitivamente, ¿Seguro Eliminar las facturas?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (rta == DialogResult.No)
                return;

            List<SqlParameter> prm = new List<SqlParameter>()
                {
                    new SqlParameter("@factura_impresa", System.Data.SqlTypes.SqlString.Null),
                    new SqlParameter("@all", "ALL"),
                    new SqlParameter("@dfecha_d", dtpDesde.Value),
                    new SqlParameter("@dfecha_h", dtpHasta.Value),
                };

            E_Resp eliDT = DataConection.DeleteRecord("pEliminarRegistro", 3, prm);
            if (!eliDT.Status)
            {
                MessageBox.Show("No se pudo eliminar los registros \n" + eliDT.errorMessage,
                     "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                Cursor.Current = Cursors.Default;
                return;
            }
            else
            {
                dgvData.Rows.RemoveAt(dgvData.CurrentRow.Index);
                MessageBox.Show("Registros eliminados exitosamente!", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }

            butClean_Click(null, null);

        }
    }
}
