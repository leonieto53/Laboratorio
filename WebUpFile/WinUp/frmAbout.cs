﻿using Business_Up;
using Entity_Up;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;


namespace WinUp
{
    public partial class frmAbout : Form
    {

        public frmAbout()
        {
            InitializeComponent();
        }

        public IFormMain frmMain { get; set; }

        private int posY = 0;
        private int posX = 0;

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            MovePanel(e);
        }

        private void frmSetup_Load(object sender, EventArgs e)
        {
        }

        private void MovePanel(MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                posX = e.X;
                posY = e.Y;
            }
            else
            {
                Left = Left + (e.X - posX);
                Top = Top + (e.Y - posY);
            }
        }

        private void butExit_Click(object sender, EventArgs e)
        {
            frmMain form = new frmMain();
            form.SetbutSetup = Color.FromArgb(221, 234, 239);
            
            this.Close();
        }


        private void butMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
            this.frmMain.ChangeImageFoot(picIcon.Image, "5");
            this.frmMain.backColorButton();
        }

        private void frmConection_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.frmMain.backColorButton();
        }

        private void frmConection_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void frmConection_MouseClick(object sender, MouseEventArgs e)
        {
            if (ActiveMdiChild == null)
            {
                this.BringToFront();
                this.frmMain.backColorButton();
                this.frmMain.frontColorButton(2);
            }
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            frmConection_MouseClick(null, null);
        }

    }
}
