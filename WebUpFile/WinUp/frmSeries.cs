﻿using Business_Up;
using Entity_Up;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;


namespace WinUp
{
    public partial class frmSeries : Form
    {

        public frmSeries()
        {
            InitializeComponent();
        }

        public IFormMain frmMain { get; set; }

        private int posY = 0;
        private int posX = 0;

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            MovePanel(e);
        }

        private void frmSetup_Load(object sender, EventArgs e)
        {
            Fill_dgv();
        }

        private void Fill_dgv()
        {
            E_Resp serieDT = DataConection.ConsultaIBetel("pSeleccionarSerie", 3);
            if (!serieDT.Status)
            {
                dgvData.DataSource = null;
                MessageBox.Show("No se registros en la base de datos\n" + serieDT.errorMessage,
                     "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                Cursor.Current = Cursors.Default;
                return;
            }
            dgvData.DataSource = serieDT.Obj;
            dgvData.Columns[2].Visible = false;
            dgvData.Columns[0].Width = 80; // RIF
            dgvData.Columns[1].Width = 402; // Nombre Cliente
            dgvData.Columns[0].ReadOnly = true;
            
        }

        private void MovePanel(MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                posX = e.X;
                posY = e.Y;
            }
            else
            {
                Left = Left + (e.X - posX);
                Top = Top + (e.Y - posY);
            }
        }

        private void butExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmConection_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void butMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
            this.frmMain.ChangeImageFoot(picIcon.Image, "2");
            this.frmMain.backColorButton();
        }

        private void dgvData_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            ((DataGridViewTextBoxColumn)dgvData.Columns[0]).MaxInputLength = 3; // IdSerie
            ((DataGridViewTextBoxColumn)dgvData.Columns[1]).MaxInputLength = 50; // DescrIption
        }

        private void dgvData_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvData.Rows.Count == 0) return;

            if (e.ColumnIndex == 1)
            {
                string ValueCell = dgvData.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                string IdSerie = dgvData.Rows[e.RowIndex].Cells[0].Value.ToString();

                List<SqlParameter> prm = new List<SqlParameter>()
                            {
                                new SqlParameter("@sIdSerie", IdSerie),
                                new SqlParameter("@sDescription", ValueCell)
                            };

                E_Resp updateDT = DataConection.ActualizarTabla("pModificaSeries", 3, prm);
                if (!updateDT.Status)
                    MessageBox.Show("Error al actualizar los cambios \n" + updateDT.errorMessage,
                                             "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmAddRecordsSerie frm = new frmAddRecordsSerie();
            frm.ShowDialog();
            Fill_dgv();
        }

        private void butEliminar_Click(object sender, EventArgs e)
        {
            if (dgvData.Rows.Count == 0) return;

            string ValueCell = dgvData.Rows[dgvData.CurrentRow.Index].Cells[0].Value.ToString();
            List<SqlParameter> prm = new List<SqlParameter>()
                {
                    new SqlParameter("@IdSerie", ValueCell.Trim()),
                };

            DialogResult rta = MessageBox.Show("¿Seguro de eliminar Serie: "+ValueCell+" ?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (rta == DialogResult.No)
                return;

            E_Resp facDT = DataConection.DeleteRecord("pEliminarRegistroSeries", 3, prm);
            if (!facDT.Status)
            {
                MessageBox.Show("No se pudo eliminar el registro \n" + facDT.errorMessage,
                     "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                Cursor.Current = Cursors.Default;
                return;
            }
            else
            {
                dgvData.Rows.RemoveAt(dgvData.CurrentRow.Index);
                MessageBox.Show("Registro eliminado exitosamente!", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
        }
    }
}
