﻿namespace WinUp
{
    partial class frmUpload
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.butMinimize = new System.Windows.Forms.Button();
            this.picIcon = new System.Windows.Forms.PictureBox();
            this.labelMenu1 = new System.Windows.Forms.Label();
            this.butExit = new System.Windows.Forms.Button();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTotalRecords = new System.Windows.Forms.TextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.dtpDesde = new System.Windows.Forms.DateTimePicker();
            this.dtpHasta = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.radioAll = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.butDeteleRecords = new System.Windows.Forms.Button();
            this.butClean = new System.Windows.Forms.Button();
            this.butFind = new System.Windows.Forms.Button();
            this.radioSuc = new System.Windows.Forms.RadioButton();
            this.radioPpal = new System.Windows.Forms.RadioButton();
            this.txtTotalFacturas = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.checkMostrarColumnas = new System.Windows.Forms.CheckBox();
            this.butEliminar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.butEncabezado = new System.Windows.Forms.Button();
            this.butSave = new System.Windows.Forms.Button();
            this.butTest = new System.Windows.Forms.Button();
            this.butAlgoritmo = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.panel1.Controls.Add(this.butMinimize);
            this.panel1.Controls.Add(this.picIcon);
            this.panel1.Controls.Add(this.labelMenu1);
            this.panel1.Controls.Add(this.butExit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1106, 40);
            this.panel1.TabIndex = 0;
            this.panel1.Click += new System.EventHandler(this.panel1_Click);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // butMinimize
            // 
            this.butMinimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butMinimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.butMinimize.FlatAppearance.BorderSize = 0;
            this.butMinimize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(171)))), ((int)(((byte)(171)))));
            this.butMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butMinimize.Image = global::WinUp.Properties.Resources.guionNegro;
            this.butMinimize.Location = new System.Drawing.Point(1024, 11);
            this.butMinimize.Name = "butMinimize";
            this.butMinimize.Size = new System.Drawing.Size(30, 23);
            this.butMinimize.TabIndex = 9;
            this.butMinimize.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butMinimize.UseVisualStyleBackColor = false;
            this.butMinimize.Click += new System.EventHandler(this.butMinimize_Click);
            // 
            // picIcon
            // 
            this.picIcon.Image = global::WinUp.Properties.Resources.Subir3;
            this.picIcon.Location = new System.Drawing.Point(11, 6);
            this.picIcon.Name = "picIcon";
            this.picIcon.Size = new System.Drawing.Size(31, 28);
            this.picIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picIcon.TabIndex = 8;
            this.picIcon.TabStop = false;
            // 
            // labelMenu1
            // 
            this.labelMenu1.AutoSize = true;
            this.labelMenu1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMenu1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.labelMenu1.Location = new System.Drawing.Point(46, 12);
            this.labelMenu1.Name = "labelMenu1";
            this.labelMenu1.Size = new System.Drawing.Size(126, 18);
            this.labelMenu1.TabIndex = 7;
            this.labelMenu1.Text = "Procesar facturas";
            // 
            // butExit
            // 
            this.butExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.butExit.FlatAppearance.BorderSize = 0;
            this.butExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.butExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butExit.Image = global::WinUp.Properties.Resources.x_salida_Negro16;
            this.butExit.Location = new System.Drawing.Point(1059, 7);
            this.butExit.Margin = new System.Windows.Forms.Padding(4);
            this.butExit.Name = "butExit";
            this.butExit.Size = new System.Drawing.Size(40, 28);
            this.butExit.TabIndex = 7;
            this.butExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butExit.UseVisualStyleBackColor = false;
            this.butExit.Click += new System.EventHandler(this.butExit_Click);
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.AllowUserToDeleteRows = false;
            this.dgvData.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Location = new System.Drawing.Point(12, 196);
            this.dgvData.Name = "dgvData";
            this.dgvData.Size = new System.Drawing.Size(1082, 392);
            this.dgvData.TabIndex = 8;
            this.dgvData.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellEnter);
            this.dgvData.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellValueChanged);
            this.dgvData.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvData_EditingControlShowing);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.label7.Location = new System.Drawing.Point(601, 173);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 18);
            this.label7.TabIndex = 18;
            this.label7.Text = "Total Facturas: ";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtTotalRecords
            // 
            this.txtTotalRecords.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTotalRecords.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalRecords.Location = new System.Drawing.Point(714, 175);
            this.txtTotalRecords.MaxLength = 30;
            this.txtTotalRecords.Name = "txtTotalRecords";
            this.txtTotalRecords.ReadOnly = true;
            this.txtTotalRecords.Size = new System.Drawing.Size(79, 17);
            this.txtTotalRecords.TabIndex = 19;
            this.txtTotalRecords.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.label1.Location = new System.Drawing.Point(8, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 18);
            this.label1.TabIndex = 30;
            this.label1.Text = "Fecha desde:";
            // 
            // dtpDesde
            // 
            this.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDesde.Location = new System.Drawing.Point(112, 30);
            this.dtpDesde.Name = "dtpDesde";
            this.dtpDesde.Size = new System.Drawing.Size(103, 22);
            this.dtpDesde.TabIndex = 1;
            // 
            // dtpHasta
            // 
            this.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpHasta.Location = new System.Drawing.Point(291, 30);
            this.dtpHasta.Name = "dtpHasta";
            this.dtpHasta.Size = new System.Drawing.Size(103, 22);
            this.dtpHasta.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.label4.Location = new System.Drawing.Point(237, 32);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 18);
            this.label4.TabIndex = 32;
            this.label4.Text = "Hasta:";
            // 
            // radioAll
            // 
            this.radioAll.AutoSize = true;
            this.radioAll.Location = new System.Drawing.Point(340, 71);
            this.radioAll.Name = "radioAll";
            this.radioAll.Size = new System.Drawing.Size(66, 20);
            this.radioAll.TabIndex = 5;
            this.radioAll.TabStop = true;
            this.radioAll.Text = "Todas";
            this.radioAll.UseVisualStyleBackColor = true;
            this.radioAll.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.butDeteleRecords);
            this.groupBox1.Controls.Add(this.butClean);
            this.groupBox1.Controls.Add(this.butFind);
            this.groupBox1.Controls.Add(this.radioSuc);
            this.groupBox1.Controls.Add(this.radioPpal);
            this.groupBox1.Controls.Add(this.radioAll);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dtpDesde);
            this.groupBox1.Controls.Add(this.dtpHasta);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.groupBox1.Location = new System.Drawing.Point(12, 55);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(568, 114);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtros de busqueda";
            // 
            // butDeteleRecords
            // 
            this.butDeteleRecords.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.butDeteleRecords.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butDeteleRecords.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butDeteleRecords.FlatAppearance.BorderSize = 0;
            this.butDeteleRecords.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.butDeteleRecords.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butDeteleRecords.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butDeteleRecords.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butDeteleRecords.Image = global::WinUp.Properties.Resources.x_salida_Negro16;
            this.butDeteleRecords.Location = new System.Drawing.Point(11, 65);
            this.butDeteleRecords.Name = "butDeteleRecords";
            this.butDeteleRecords.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.butDeteleRecords.Size = new System.Drawing.Size(165, 39);
            this.butDeteleRecords.TabIndex = 36;
            this.butDeteleRecords.Text = "Borrar selección";
            this.butDeteleRecords.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butDeteleRecords.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butDeteleRecords.UseVisualStyleBackColor = false;
            this.butDeteleRecords.Click += new System.EventHandler(this.butDeteleRecords_Click);
            // 
            // butClean
            // 
            this.butClean.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.butClean.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butClean.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butClean.FlatAppearance.BorderSize = 0;
            this.butClean.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.butClean.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butClean.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butClean.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butClean.Image = global::WinUp.Properties.Resources.Broom_2;
            this.butClean.Location = new System.Drawing.Point(419, 65);
            this.butClean.Name = "butClean";
            this.butClean.Size = new System.Drawing.Size(119, 39);
            this.butClean.TabIndex = 35;
            this.butClean.Text = " Limpiar";
            this.butClean.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butClean.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butClean.UseVisualStyleBackColor = false;
            this.butClean.Click += new System.EventHandler(this.butClean_Click);
            // 
            // butFind
            // 
            this.butFind.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.butFind.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butFind.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butFind.FlatAppearance.BorderSize = 0;
            this.butFind.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.butFind.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butFind.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butFind.Image = global::WinUp.Properties.Resources.buscarAzul24;
            this.butFind.Location = new System.Drawing.Point(419, 17);
            this.butFind.Name = "butFind";
            this.butFind.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            this.butFind.Size = new System.Drawing.Size(119, 39);
            this.butFind.TabIndex = 6;
            this.butFind.Text = " Buscar";
            this.butFind.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butFind.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butFind.UseVisualStyleBackColor = false;
            this.butFind.Click += new System.EventHandler(this.butFind_Click_1);
            // 
            // radioSuc
            // 
            this.radioSuc.AutoSize = true;
            this.radioSuc.Location = new System.Drawing.Point(260, 71);
            this.radioSuc.Name = "radioSuc";
            this.radioSuc.Size = new System.Drawing.Size(78, 20);
            this.radioSuc.TabIndex = 4;
            this.radioSuc.TabStop = true;
            this.radioSuc.Text = "Sucursal";
            this.radioSuc.UseVisualStyleBackColor = true;
            this.radioSuc.Visible = false;
            // 
            // radioPpal
            // 
            this.radioPpal.AutoSize = true;
            this.radioPpal.Location = new System.Drawing.Point(182, 71);
            this.radioPpal.Name = "radioPpal";
            this.radioPpal.Size = new System.Drawing.Size(78, 20);
            this.radioPpal.TabIndex = 3;
            this.radioPpal.TabStop = true;
            this.radioPpal.Text = "Principal";
            this.radioPpal.UseVisualStyleBackColor = true;
            this.radioPpal.Visible = false;
            // 
            // txtTotalFacturas
            // 
            this.txtTotalFacturas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTotalFacturas.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalFacturas.Location = new System.Drawing.Point(890, 175);
            this.txtTotalFacturas.MaxLength = 30;
            this.txtTotalFacturas.Name = "txtTotalFacturas";
            this.txtTotalFacturas.ReadOnly = true;
            this.txtTotalFacturas.Size = new System.Drawing.Size(181, 17);
            this.txtTotalFacturas.TabIndex = 32;
            this.txtTotalFacturas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.label2.Location = new System.Drawing.Point(806, 173);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 18);
            this.label2.TabIndex = 31;
            this.label2.Text = "Total Valor: ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.label3.Location = new System.Drawing.Point(16, 173);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 18);
            this.label3.TabIndex = 33;
            this.label3.Text = "Mostrar Columnas: ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // checkMostrarColumnas
            // 
            this.checkMostrarColumnas.AutoSize = true;
            this.checkMostrarColumnas.Location = new System.Drawing.Point(154, 175);
            this.checkMostrarColumnas.Name = "checkMostrarColumnas";
            this.checkMostrarColumnas.Size = new System.Drawing.Size(15, 14);
            this.checkMostrarColumnas.TabIndex = 34;
            this.checkMostrarColumnas.UseVisualStyleBackColor = true;
            this.checkMostrarColumnas.CheckedChanged += new System.EventHandler(this.checkMostrarColumnas_CheckedChanged);
            // 
            // butEliminar
            // 
            this.butEliminar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
            this.butEliminar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butEliminar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butEliminar.FlatAppearance.BorderSize = 0;
            this.butEliminar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.butEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butEliminar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butEliminar.Image = global::WinUp.Properties.Resources.Eliminar_Red32;
            this.butEliminar.Location = new System.Drawing.Point(1058, 136);
            this.butEliminar.Name = "butEliminar";
            this.butEliminar.Size = new System.Drawing.Size(36, 33);
            this.butEliminar.TabIndex = 38;
            this.butEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butEliminar.UseVisualStyleBackColor = false;
            this.butEliminar.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.button1.Image = global::WinUp.Properties.Resources.Agregar_azul_claro32;
            this.button1.Location = new System.Drawing.Point(1018, 136);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(36, 33);
            this.button1.TabIndex = 37;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // butEncabezado
            // 
            this.butEncabezado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.butEncabezado.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butEncabezado.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butEncabezado.FlatAppearance.BorderSize = 0;
            this.butEncabezado.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.butEncabezado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butEncabezado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butEncabezado.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butEncabezado.Image = global::WinUp.Properties.Resources.Customer32_azul;
            this.butEncabezado.Location = new System.Drawing.Point(711, 114);
            this.butEncabezado.Name = "butEncabezado";
            this.butEncabezado.Size = new System.Drawing.Size(109, 30);
            this.butEncabezado.TabIndex = 27;
            this.butEncabezado.Text = "Clientes";
            this.butEncabezado.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butEncabezado.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butEncabezado.UseVisualStyleBackColor = false;
            this.butEncabezado.Visible = false;
            this.butEncabezado.Click += new System.EventHandler(this.butEncabezado_Click);
            // 
            // butSave
            // 
            this.butSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.butSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butSave.FlatAppearance.BorderSize = 0;
            this.butSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.butSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butSave.Image = global::WinUp.Properties.Resources.salvarAzul24;
            this.butSave.Location = new System.Drawing.Point(913, 60);
            this.butSave.Name = "butSave";
            this.butSave.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            this.butSave.Size = new System.Drawing.Size(182, 42);
            this.butSave.TabIndex = 6;
            this.butSave.Text = "    Guadar Profit";
            this.butSave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butSave.UseVisualStyleBackColor = false;
            this.butSave.Click += new System.EventHandler(this.butSave_Click);
            // 
            // butTest
            // 
            this.butTest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.butTest.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butTest.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butTest.FlatAppearance.BorderSize = 0;
            this.butTest.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.butTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butTest.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butTest.Image = global::WinUp.Properties.Resources.procesoAzul24;
            this.butTest.Location = new System.Drawing.Point(711, 60);
            this.butTest.Name = "butTest";
            this.butTest.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            this.butTest.Size = new System.Drawing.Size(182, 42);
            this.butTest.TabIndex = 7;
            this.butTest.Text = "    Validar";
            this.butTest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butTest.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butTest.UseVisualStyleBackColor = false;
            this.butTest.Click += new System.EventHandler(this.butTest_Click);
            // 
            // butAlgoritmo
            // 
            this.butAlgoritmo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.butAlgoritmo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butAlgoritmo.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butAlgoritmo.FlatAppearance.BorderSize = 0;
            this.butAlgoritmo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(223)))), ((int)(((byte)(130)))));
            this.butAlgoritmo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butAlgoritmo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butAlgoritmo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(79)))), ((int)(((byte)(116)))));
            this.butAlgoritmo.Image = global::WinUp.Properties.Resources.encabezadoAzul24;
            this.butAlgoritmo.Location = new System.Drawing.Point(839, 114);
            this.butAlgoritmo.Name = "butAlgoritmo";
            this.butAlgoritmo.Size = new System.Drawing.Size(108, 30);
            this.butAlgoritmo.TabIndex = 39;
            this.butAlgoritmo.Text = "Facturas";
            this.butAlgoritmo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butAlgoritmo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butAlgoritmo.UseVisualStyleBackColor = false;
            this.butAlgoritmo.Visible = false;
            this.butAlgoritmo.Click += new System.EventHandler(this.butAlgoritmo_Click);
            // 
            // frmUpload
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(1106, 607);
            this.Controls.Add(this.butAlgoritmo);
            this.Controls.Add(this.butEliminar);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checkMostrarColumnas);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtTotalFacturas);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.butEncabezado);
            this.Controls.Add(this.txtTotalRecords);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.butSave);
            this.Controls.Add(this.butTest);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(30, 30);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmUpload";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmUpload_FormClosing);
            this.Load += new System.EventHandler(this.frmSetup_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmUpload_KeyDown);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.frmUpload_MouseClick);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button butExit;
        private System.Windows.Forms.Label labelMenu1;
        private System.Windows.Forms.PictureBox picIcon;
        private System.Windows.Forms.Button butTest;
        private System.Windows.Forms.Button butSave;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTotalRecords;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button butEncabezado;
        private System.Windows.Forms.Button butMinimize;
        //private System.Windows.Forms.TextBox txtEnterprice;
        private System.Windows.Forms.DateTimePicker dtpHasta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpDesde;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioAll;
        private System.Windows.Forms.RadioButton radioSuc;
        private System.Windows.Forms.RadioButton radioPpal;
        private System.Windows.Forms.Button butFind;
        private System.Windows.Forms.TextBox txtTotalFacturas;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkMostrarColumnas;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button butClean;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button butEliminar;
        private System.Windows.Forms.Button butAlgoritmo;
        private System.Windows.Forms.Button butDeteleRecords;
    }
}