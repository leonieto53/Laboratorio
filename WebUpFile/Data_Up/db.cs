﻿using Entity_Up;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace Data_Up
{
    public class db
    {
        private static XmlDocument doc;

        public static string ConectionString(int db)
        {
            E_XmlFields xml = ReadXml();
            string _server = string.Empty;
            string _database = string.Empty;
            string _user = string.Empty;
            string _password = string.Empty;

            _database = xml.c_database;
            if (db == 1)
            {
                _server = xml.c_server;
                _database = xml.c_database;
                _user = xml.c_user;
                _password = xml.c_password;
            }
            else if (db == 3)
            {
                _server = xml.c_server2;
                _database = xml.c_database2;
                _user = xml.c_user2;
                _password = xml.c_password2;
            }

            string cs = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};",
                            _server, _database, _user, _password);
            return cs;
        }

        public static E_XmlFields ReadXml()
        {
            string xlmnamec = Application.StartupPath + @"\Probia.Config\Probia.Configuration.xml";
            string xlmnamep = Application.StartupPath + @"\Probia.Config\Probia.Parameters.xml";
            E_Resp respond = new E_Resp();
            E_XmlFields field = new E_XmlFields();

            doc = new XmlDocument();

            try
            {
                // read Conection
                doc.Load(xlmnamec);
                XmlNodeList route = doc.SelectNodes("ProbiaConfigurations/MasterConectionString");
                XmlNode subnode = route.Item(0);
                field.c_server = subnode.SelectSingleNode("ServerName").InnerText;
                field.c_database = subnode.SelectSingleNode("bdname").InnerText;
                field.c_master = subnode.SelectSingleNode("master").InnerText;
                field.c_user = subnode.SelectSingleNode("user").InnerText;
                field.c_password = subnode.SelectSingleNode("password").InnerText;
                field.c_server2 = subnode.SelectSingleNode("ServerName2").InnerText;
                field.c_database2 = subnode.SelectSingleNode("bdname2").InnerText;
                field.c_user2 = subnode.SelectSingleNode("user2").InnerText;
                field.c_password2 = subnode.SelectSingleNode("password2").InnerText;
                // read Parameters
                doc.Load(xlmnamep);
                route = doc.SelectNodes("ProbiaParameters/Parameters");
                subnode = route.Item(0);
                field.p_transport = subnode.SelectSingleNode("Transport").InnerText;
                field.p_warehouse = subnode.SelectSingleNode("Warehouse").InnerText;
                field.p_priceType = subnode.SelectSingleNode("PriceType").InnerText;
                field.p_ivatype = subnode.SelectSingleNode("IvaType").InnerText;
                field.p_user = subnode.SelectSingleNode("User").InnerText;
                field.p_money = subnode.SelectSingleNode("Moneda").InnerText;
                field.p_principal = subnode.SelectSingleNode("Principal").InnerText;
                field.p_customer = subnode.SelectSingleNode("Customer").InnerText;
                field.p_sucursal = subnode.SelectSingleNode("Sucursal").InnerText;
                field.p_artp = subnode.SelectSingleNode("ArtP").InnerText;
                field.p_arts = subnode.SelectSingleNode("ArtS").InnerText;

                field.ok = true;
                return field;
            }
            catch(Exception ex)
            {
                field.ok = false;
                field.message = ex.Message;
                return field;
            }
        }

        public static E_Resp TestConecion(string conectionstring)
        {
            E_Resp respond = new E_Resp();

            try
            {
                using (SqlConnection cn = new SqlConnection(conectionstring))
                {
                    if (cn.State == ConnectionState.Closed)
                    {
                        cn.Open();
                        respond.Message = "OK";
                        respond.Status = true;
                    }
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                respond.Message = "Error al probar la conexion con la base de datos";
                respond.Status = false;
                respond.errorMessage = ex.Message;
            }

            return respond;
        }

        public static E_Resp consultar(string storeprocedure, int db )
        {
            E_Resp respond = new E_Resp();

            try
            {
                string conectionstring = ConectionString(db);

                using (SqlConnection sql = new SqlConnection(conectionstring))
                {
                    using (SqlCommand cmd = new SqlCommand(storeprocedure, sql))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.Add(new SqlParameter("@pais", idPais));
                        DataTable dt = new DataTable();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        sql.Open();
                        da.Fill(dt);

                        respond.Message = "OK";
                        respond.Status = true;
                        respond.Obj = dt;
                        sql.Close();

                        return respond;
                    }
                }
                
            }
            catch (Exception ex)
            {
                respond.Message = "Error al consultar Encabezado Cotizacion";
                respond.Status = false;
                respond.errorMessage = ex.Message;
                respond.errorMessage = ex.Message;

                return respond;
            }
        }

        public static E_Resp Exist(string storeprocedure, int db, string parameter, string strfind)
        {
            E_Resp respond = new E_Resp();
 
            try
            {
                string conectionstring = ConectionString(db);

                using (SqlConnection sql = new SqlConnection(conectionstring))
                {
                    using (SqlCommand cmd = new SqlCommand(storeprocedure, sql))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter(parameter, strfind));
                        DataTable dt = new DataTable();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        sql.Open();
                        da.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            respond.Message = "OK";
                            respond.Status = true;
                            respond.Obj = dt;
                        }
                        else
                        {
                            respond.Message = "FAIL";
                            respond.Status = false;
                            respond.Obj = null;
                        }
                        sql.Close();

                        return respond;
                    }
                }
            }
            catch (Exception ex)
            {
                respond.Message = "Error al consultar busqueda.";
                respond.Status = false;
                respond.errorMessage = ex.Message;

                return respond;
            }
        }

        public static E_Resp NextConsecutive(string storeprocedure, int db, List<SqlParameter> parameters)
        {
            E_Resp respond = new E_Resp();

            try
            {
                string conectionstring = ConectionString(db);

                using (SqlConnection sql = new SqlConnection(conectionstring))
                {
                    using (SqlCommand cmd = new SqlCommand(storeprocedure, sql))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(parameters.ToArray());
                        DataTable dt = new DataTable();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        sql.Open();
                        da.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            respond.Message = "OK";
                            respond.Status = true;
                            respond.Obj = dt;
                        }
                        else
                        {
                            respond.Message = "FAIL";
                            respond.Status = false;
                            respond.Obj = null;
                        }
                        cmd.Parameters.Clear();
                        sql.Close();
                        return respond;
                    }
                }
            }
            catch (Exception ex)
            {
                respond.Message = "Error al consultar busqueda.";
                respond.Status = false;
                respond.errorMessage = ex.Message;
                
                return respond;
            }
        }

        public static E_Resp ActualizarTabla(string storeprocedure, int db, List<SqlParameter> parameters)
        {
            E_Resp respond = new E_Resp();

            try
            {
                string conectionstring = ConectionString(db);

                using (SqlConnection sql = new SqlConnection(conectionstring))
                {
                    using (SqlCommand cmd = new SqlCommand(storeprocedure, sql))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(parameters.ToArray());
                        DataTable dt = new DataTable();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        sql.Open();
                        int rowsAffected = cmd.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            respond.Message = "OK";
                            respond.Status = true;
                            respond.Obj = dt;
                        }
                        else
                        {
                            respond.Message = "FAIL";
                            respond.Status = false;
                            respond.Obj = null;
                        }
                        cmd.Parameters.Clear();
                        sql.Close();
                        return respond;
                    }
                }
            }
            catch (Exception ex)
            {
                respond.Message = "No se pudo actualizar en la base de datos.";
                respond.Status = false;
                respond.errorMessage = ex.Message;

                return respond;
            }
        }

        public static E_Resp InsertRecord(string storeprocedure, int db, List<SqlParameter> parameters)
        {
            E_Resp respond = new E_Resp();

            try
            {
                string conectionstring = ConectionString(db);

                using (SqlConnection sql = new SqlConnection(conectionstring))
                {
                    using (SqlCommand cmd = new SqlCommand(storeprocedure, sql))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(parameters.ToArray());
                        DataTable dt = new DataTable();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        sql.Open();
                        int rowsAffected = cmd.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            respond.Message = "OK";
                            respond.Status = true;
                            respond.Obj = dt;
                        }
                        else
                        {
                            respond.Message = "FAIL";
                            respond.Status = false;
                            respond.Obj = null;
                        }
                        cmd.Parameters.Clear();
                        sql.Close();
                        return respond;
                    }
                }
            }
            catch (Exception ex)
            {
                respond.Message = "No se pudo insertar registro en la base de datos.";
                respond.Status = false;
                respond.errorMessage = ex.Message;

                return respond;
            }
        }

        public static E_Resp DeleteRecord(string storeprocedure, int db, List<SqlParameter> parameters)
        {
            E_Resp respond = new E_Resp();

            try
            {
                string conectionstring = ConectionString(db);

                using (SqlConnection sql = new SqlConnection(conectionstring))
                {
                    using (SqlCommand cmd = new SqlCommand(storeprocedure, sql))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(parameters.ToArray());
                        DataTable dt = new DataTable();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        sql.Open();
                        int rowsAffected = cmd.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            respond.Message = "OK";
                            respond.Status = true;
                            respond.Obj = dt;
                        }
                        else
                        {
                            respond.Message = "FAIL";
                            respond.Status = false;
                            respond.Obj = null;
                        }
                        cmd.Parameters.Clear();
                        sql.Close();
                        return respond;
                    }
                }
            }
            catch (Exception ex)
            {
                respond.Message = "No se pudo eliminar el registro en la base de datos.";
                respond.Status = false;
                respond.errorMessage = ex.Message;

                return respond;
            }
        }


        public static E_Resp ConsultaIBetel(string storeprocedure, int db, List<SqlParameter> parameters = null)
        {
            E_Resp respond = new E_Resp();

            try
            {
                string conectionstring = ConectionString(db);

                using (SqlConnection sql = new SqlConnection(conectionstring))
                {
                    using (SqlCommand cmd = new SqlCommand(storeprocedure, sql))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        if(parameters != null)
                            cmd.Parameters.AddRange(parameters.ToArray());
                        DataTable dt = new DataTable();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        sql.Open();
                        da.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            respond.Message = "OK";
                            respond.Status = true;
                            respond.Obj = dt;
                        }
                        else
                        {
                            respond.Message = "NO RECORDS";
                            respond.Status = false;
                            respond.Obj = null;
                        }
                        cmd.Parameters.Clear();
                        sql.Close();
                        return respond;
                    }
                }
            }
            catch (Exception ex)
            {
                respond.Message = "Error al consultar en la base de datos.";
                respond.Status = false;
                respond.errorMessage = ex.Message;

                return respond;
            }
        }


        public static E_Resp InsertCotizacionHeadAll(List<E_CotizacionHead> heads, string subsidiary, 
            List<E_CotizacionDetail> details, bool subs, List<E_DocumentoHead> documents)
        {
            E_Resp respond = new E_Resp();
            string prox = "";
            string useSub = "";


            string conectionstring = ConectionString(1);

            using (SqlConnection sql = new SqlConnection(conectionstring))
            {
                sql.Open();

                using (SqlTransaction transaction = sql.BeginTransaction())
                {

                    try
                    {
                        //// Verifica si maneja sucursal
                        //if(subs)
                        //{
                        //    useSub = subsidiary;
                        //}
                        //else
                        //{
                        //    useSub = "";
                        //}

                        foreach (E_CotizacionHead head in heads)
                        {
                            prox = "";
                            // trae y actualiza consecutivo 
                            List<SqlParameter> prm = new List<SqlParameter>()
                            {
                                //new SqlParameter("@sCo_Sucur", subsidiary),
                                new SqlParameter("@sCo_Sucur", useSub),
                                new SqlParameter("sCo_Consecutivo", "DOC_VEN_FACT")
                            };

                            using (SqlCommand cmd = new SqlCommand("pConsecutivoProximo", sql))
                            {
                                cmd.Transaction = transaction;
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddRange(prm.ToArray());
                                DataTable dt = new DataTable();
                                SqlDataAdapter da = new SqlDataAdapter(cmd);
                                da.Fill(dt);

                                if (dt.Rows.Count > 0)
                                {
                                   prox = dt.Rows[0]["ProximoConsecutivo"].ToString();
                                    head.sDoc_Num = prox;
                                    //// document
                                    //document.sNro_Orig
                                    //document.document.sNro_Doc
                                }
                                cmd.Parameters.Clear();
                            }

                            // graba Encabezado   5789
                            using (SqlCommand cmd = new SqlCommand("pInsertarFacturaVenta", sql))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add("@sDoc_Num", SqlDbType.Char, (20)).Value = head.sDoc_Num;
                                cmd.Parameters.Add("@sDescrip", SqlDbType.VarChar, (60)).Value = (head.sDescrip == null ? System.Data.SqlTypes.SqlString.Null : head.sDescrip); 
                                cmd.Parameters.Add("@sCo_Cli", SqlDbType.Char, (16)).Value = head.sCo_Cli;
                                cmd.Parameters.Add("@sCo_Cta_Ingr_Egr", SqlDbType.Char, (20)).Value = head.sCo_Cta_Ingr_Egr;
                                cmd.Parameters.Add("@sCo_Tran", SqlDbType.Char, (6)).Value = head.sCo_Tran;
                                cmd.Parameters.Add("@sCo_Mone", SqlDbType.Char, (6)).Value = head.sCo_Mone;
                                cmd.Parameters.Add("@sCo_Ven", SqlDbType.Char, (6)).Value = head.sCo_Ven;
                                cmd.Parameters.Add("@sCo_Cond", SqlDbType.Char, (6)).Value = head.sCo_Cond;
                                cmd.Parameters.Add("@sdFec_Emis", SqlDbType.SmallDateTime).Value = head.sdFec_Emis;
                                cmd.Parameters.Add("@sdFec_Venc", SqlDbType.SmallDateTime).Value = head.sdFec_Venc;
                                cmd.Parameters.Add("@sdFec_Reg", SqlDbType.SmallDateTime).Value = head.sdFec_Reg;
                                cmd.Parameters.Add("@bAnulado", SqlDbType.Bit).Value = head.bAnulado;
                                cmd.Parameters.Add("@sStatus", SqlDbType.Char, (1)).Value = head.sStatus;
                                cmd.Parameters.Add("@deTasa", SqlDbType.Decimal).Value = head.deTasa;
                                cmd.Parameters.Add("@sN_Control", SqlDbType.VarChar, (20)).Value = System.Data.SqlTypes.SqlDateTime.Null;//head.sN_Control;
                                //cmd.Parameters.Add("@sNro_Doc", SqlDbType.VarChar, (20)).Value = head.sNro_Doc;
                                cmd.Parameters.Add("@sPorc_Desc_Glob", SqlDbType.VarChar, (15)).Value = head.sPorc_Desc_Glob;
                                cmd.Parameters.Add("@deMonto_Desc_Glob", SqlDbType.Decimal).Value = head.deMonto_Desc_Glob;
                                cmd.Parameters.Add("@sPorc_Reca", SqlDbType.VarChar, (15)).Value = head.sPorc_Reca;
                                cmd.Parameters.Add("@deMonto_Reca", SqlDbType.Decimal).Value = head.deMonto_Reca;
                                cmd.Parameters.Add("@deSaldo", SqlDbType.Decimal).Value = head.deSaldo;
                                cmd.Parameters.Add("@deTotal_Bruto", SqlDbType.Decimal).Value = head.deTotal_Bruto;
                                cmd.Parameters.Add("@deMonto_Imp", SqlDbType.Decimal).Value = head.deMonto_Imp;
                                cmd.Parameters.Add("@deMonto_Imp2", SqlDbType.Decimal).Value = head.deMonto_Imp2;
                                cmd.Parameters.Add("@deMonto_Imp3", SqlDbType.Decimal).Value = head.deMonto_Imp3;
                                cmd.Parameters.Add("@deOtros1", SqlDbType.Decimal).Value = head.deOtros1;
                                cmd.Parameters.Add("@deOtros2", SqlDbType.Decimal).Value = head.deOtros2;
                                cmd.Parameters.Add("@deOtros3", SqlDbType.Decimal).Value = head.deOtros3;
                                cmd.Parameters.Add("@deTotal_Neto", SqlDbType.Decimal).Value = head.deTotal_Neto;
                                cmd.Parameters.Add("@sDis_Cen", SqlDbType.VarChar, -1).Value = System.Data.SqlTypes.SqlDateTime.Null;//head.sDis_Cen;
                                cmd.Parameters.Add("@sComentario", SqlDbType.VarChar, -1).Value = System.Data.SqlTypes.SqlDateTime.Null;//head.sComentario;
                                cmd.Parameters.Add("@sDir_Ent", SqlDbType.VarChar, -1).Value = System.Data.SqlTypes.SqlDateTime.Null;//head.sDir_Ent;
                                cmd.Parameters.Add("@bImpresa", SqlDbType.Bit).Value = head.bImpresa;
                                cmd.Parameters.Add("@sSalestax", SqlDbType.Char, (8)).Value = System.Data.SqlTypes.SqlDateTime.Null;//head.sSalestax;
                                cmd.Parameters.Add("@sImpfis", SqlDbType.VarChar, (20)).Value = System.Data.SqlTypes.SqlDateTime.Null;//head.sImpfis;
                                cmd.Parameters.Add("@sImpfisfac", SqlDbType.VarChar, (20)).Value = System.Data.SqlTypes.SqlDateTime.Null;//head.sImpfisfac;
                                cmd.Parameters.Add("@bVen_Ter", SqlDbType.Bit).Value = head.bVen_Ter;
                                cmd.Parameters.Add("@sCampo1", SqlDbType.VarChar, (60)).Value = head.sCampo1;
                                cmd.Parameters.Add("@sCampo2", SqlDbType.VarChar, (60)).Value = head.sCampo2;
                                cmd.Parameters.Add("@sCampo3", SqlDbType.VarChar, (60)).Value = head.sCampo3;
                                cmd.Parameters.Add("@sCampo4", SqlDbType.VarChar, (60)).Value = head.sCampo4;
                                cmd.Parameters.Add("@sCampo5", SqlDbType.VarChar, (60)).Value = head.sCampo5;
                                cmd.Parameters.Add("@sCampo6", SqlDbType.VarChar, (60)).Value = head.sCampo6;
                                cmd.Parameters.Add("@sCampo7", SqlDbType.VarChar, (60)).Value = head.sCampo7;
                                cmd.Parameters.Add("@sCampo8", SqlDbType.VarChar, (60)).Value = head.sCampo8;
                                cmd.Parameters.Add("@sCo_Us_In", SqlDbType.Char, (6)).Value = head.sCo_Us_In;
                                cmd.Parameters.Add("@sCo_Sucu_In", SqlDbType.Char, (6)).Value = 
                                    (useSub == "" ? System.Data.SqlTypes.SqlString.Null : useSub);
                                cmd.Parameters.Add("@sRevisado", SqlDbType.Char, (1)).Value = head.sRevisado;
                                cmd.Parameters.Add("@sTrasnfe", SqlDbType.Char, (1)).Value = head.sTrasnfe;
                                cmd.Parameters.Add("@sMaquina", SqlDbType.VarChar, (60)).Value = head.sMaquina;
                                cmd.Parameters.Add("@bContrib", SqlDbType.Bit).Value = head.bContrib;

                                cmd.Transaction = transaction;
                                cmd.ExecuteNonQuery();

                                //****************************************************************
                                // Llama procedimiento para insertar detalle facturas

                                var detprod = details.Where(x => x.sComentario == head.sDescrip).ToList();
                                
                                foreach (E_CotizacionDetail det in detprod)
                                {
                                    det.sDoc_Num = prox;
                                    respond = InsertCotizacionDetail(det, transaction, sql, useSub);
                                    if (!respond.Status)
                                    {
                                        transaction.Rollback();
                                        respond.Message = "Error al insertar registros en detalles factura";
                                        respond.Status = false;
                                        return respond;
                                    }
                                }

                                //****************************************************************
                                // Llama procedimiento para insertar documentos
                                E_DocumentoHead doc = documents.Where(x => x.sCampo1 == head.sDescrip).FirstOrDefault();
                                doc.sNro_Doc = prox;
                                doc.sNro_Orig = prox;
                                respond = InsertDocumentHead(doc, transaction, sql, useSub);
                                if (!respond.Status)
                                {
                                    transaction.Rollback();
                                    respond.Message = "Error al insertar registros en documentos";
                                    respond.Status = false;
                                    return respond;
                                }
                            }
                        }
                        respond.Message = "OK";
                        respond.Status = true;
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        respond.Message = "Error al insertar registros en encabezado facturas";
                        respond.Status = false;
                        respond.errorMessage = ex.Message;
                    }
                    sql.Close();
                    return respond;
                }
            }
        }

        public static E_Resp Find_SameConection(SqlTransaction transaction, SqlConnection sql, string strfind)
        {
            E_Resp respond = new E_Resp();
            respond.Status = false;

            try
            {
                string conectionstring = ConectionString(1);
                // [pSeleccionarParametrosEmpresa]] nombre store procedure buscar parametros empresa
                using (SqlCommand cmd = new SqlCommand("pSeleccionarParametrosEmpresa]", sql))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@sCod_Emp", SqlDbType.VarChar).Value = strfind;
                    cmd.Transaction = transaction;
                    DataTable dt = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {
                        respond.Status = Convert.ToBoolean(dt.Rows[0]["v_maneja_sucursales"].ToString());
                    }
                    cmd.Parameters.Clear();
                }

                return respond;
            }
            catch (Exception ex)
            {
                respond.Message = "No se pudo encontrar parámetros de empresa";
                respond.Status = false;
                respond.errorMessage = ex.Message;
                return respond;
            }
            
        }

        public static E_Resp InsertCotizacionDetail(E_CotizacionDetail det, SqlTransaction transaction, SqlConnection sql, string sub)
        {
            E_Resp respond = new E_Resp();

            try
            {
                string conectionstring = ConectionString(1);

                    // [pInsertarRenglonesCotizacionCliente] nombre store procedure insertar detalle
                    using (SqlCommand cmd = new SqlCommand("pInsertarRenglonesFacturaVenta", sql))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@iReng_Num", SqlDbType.Int).Value = det.iReng_Num;
                        cmd.Parameters.Add("@sDoc_Num", SqlDbType.Char, (20)).Value = det.sDoc_Num;
                        cmd.Parameters.Add("@sCo_Art", SqlDbType.VarChar, (30)).Value = det.sCo_Art;
                        cmd.Parameters.Add("@sDes_Art", SqlDbType.VarChar, (120)).Value = det.sDes_Art;
                        cmd.Parameters.Add("@sCo_Uni", SqlDbType.Char, (6)).Value = det.sCo_Uni;
                        cmd.Parameters.Add("@sSco_Uni", SqlDbType.Char, (6)).Value = det.sSco_Uni;
                        cmd.Parameters.Add("@sCo_Alma", SqlDbType.Char, (6)).Value = det.sCo_Alma;
                        cmd.Parameters.Add("@sCo_Precio", SqlDbType.Char, (6)).Value = det.sCo_Precio;
                        cmd.Parameters.Add("@sTipo_Imp", SqlDbType.Char, (1)).Value = det.sTipo_Imp;
                        cmd.Parameters.Add("@sTipo_Imp2", SqlDbType.Char, (1)).Value = det.sTipo_Imp2;
                        cmd.Parameters.Add("@sTipo_Imp3", SqlDbType.Char, (1)).Value = det.sTipo_Imp3;
                        cmd.Parameters.Add("@deTotal_Art", SqlDbType.Decimal).Value = det.deTotal_Art;
                        cmd.Parameters.Add("@deSTotal_Art", SqlDbType.Decimal).Value = det.deSTotal_Art;
                        cmd.Parameters.Add("@dePrec_Vta", SqlDbType.Decimal).Value = det.dePrec_Vta;
                        cmd.Parameters.Add("@sPorc_Desc", SqlDbType.VarChar, (15)).Value = det.sPorc_Desc;
                        cmd.Parameters.Add("@deMonto_Desc", SqlDbType.Decimal).Value = det.deMonto_Desc;
                        cmd.Parameters.Add("@deReng_Neto", SqlDbType.Decimal).Value = det.deReng_Neto;
                        cmd.Parameters.Add("@dePendiente", SqlDbType.Decimal).Value = det.dePendiente;
                        cmd.Parameters.Add("@dePendiente2", SqlDbType.Decimal).Value = det.dePendiente2;
                        cmd.Parameters.Add("@deMonto_Desc_Glob", SqlDbType.Decimal).Value = det.deMonto_Desc_Glob;
                        cmd.Parameters.Add("@deMonto_reca_Glob", SqlDbType.Decimal).Value = det.deMonto_reca_Glob;
                        cmd.Parameters.Add("@deOtros1_glob", SqlDbType.Decimal).Value = det.deOtros1_glob;
                        cmd.Parameters.Add("@deOtros2_glob", SqlDbType.Decimal).Value = det.deOtros2_glob;
                        cmd.Parameters.Add("@deOtros3_glob", SqlDbType.Decimal).Value = det.deOtros2_glob;
                        cmd.Parameters.Add("@deMonto_imp_afec_glob", SqlDbType.Decimal).Value = det.deMonto_imp_afec_glob;
                        cmd.Parameters.Add("@deMonto_imp2_afec_glob", SqlDbType.Decimal).Value = det.deMonto_imp2_afec_glob;
                        cmd.Parameters.Add("@deMonto_imp3_afec_glob", SqlDbType.Decimal).Value = det.deMonto_imp3_afec_glob;
                        cmd.Parameters.Add("@sTipo_Doc", SqlDbType.Char, (4)).Value = System.Data.SqlTypes.SqlDateTime.Null;
                        cmd.Parameters.Add("@gRowguid_Doc", SqlDbType.UniqueIdentifier).Value = System.Data.SqlTypes.SqlString.Null;
                        cmd.Parameters.Add("@sNum_Doc", SqlDbType.VarChar, (20)).Value = System.Data.SqlTypes.SqlDateTime.Null;
                        cmd.Parameters.Add("@dePorc_Imp", SqlDbType.Decimal).Value = det.dePorc_Imp;
                        cmd.Parameters.Add("@dePorc_Imp2", SqlDbType.Decimal).Value = det.dePorc_Imp2;
                        cmd.Parameters.Add("@dePorc_Imp3", SqlDbType.Decimal).Value = det.dePorc_Imp3;
                        cmd.Parameters.Add("@deMonto_Imp", SqlDbType.Decimal).Value = det.deMonto_Imp;
                        cmd.Parameters.Add("@deMonto_Imp2", SqlDbType.Decimal).Value = det.deMonto_Imp2;
                        cmd.Parameters.Add("@deMonto_Imp3", SqlDbType.Decimal).Value = det.deMonto_Imp3;
                        cmd.Parameters.Add("@deOtros", SqlDbType.Decimal).Value = det.deOtros;
                        cmd.Parameters.Add("@deTotal_Dev", SqlDbType.Decimal).Value = det.deTotal_Dev;
                        cmd.Parameters.Add("@deMonto_Dev", SqlDbType.Decimal).Value = det.deMonto_Dev;
                        cmd.Parameters.Add("@sComentario", SqlDbType.VarChar, -1).Value = det.sComentario;
                        cmd.Parameters.Add("@sDis_Cen", SqlDbType.VarChar, -1).Value = det.sDis_Cen;
                        cmd.Parameters.Add("@sCo_Sucu_In", SqlDbType.Char, (6)).Value =
                            (sub == "" ? System.Data.SqlTypes.SqlString.Null : sub); ;
                        cmd.Parameters.Add("@sCo_Us_In", SqlDbType.Char, (6)).Value = det.sCo_Us_In;
                        cmd.Parameters.Add("@sREVISADO", SqlDbType.Char, (1)).Value = System.Data.SqlTypes.SqlDateTime.Null;
                        cmd.Parameters.Add("@sTRASNFE", SqlDbType.Char, (1)).Value = System.Data.SqlTypes.SqlDateTime.Null;
                        cmd.Parameters.Add("@sMaquina", SqlDbType.VarChar, (60)).Value = det.sMaquina;

                        cmd.Transaction = transaction;
                        cmd.ExecuteNonQuery();

                        respond.Message = "OK";
                        respond.Status = true;

                        return respond;
                    }
            }
            catch (Exception ex)
            {
                respond.Message = "Error al insertar Detalle Cotizacion";
                respond.Status = false;
                respond.errorMessage = ex.Message;

                return respond;
            }
        }

        public static E_Resp InsertDocumentHead(E_DocumentoHead det, SqlTransaction transaction, SqlConnection sql, string sub)
        {
            E_Resp respond = new E_Resp();

            try
            {
                string conectionstring = ConectionString(1);

                // [pInsertarRenglonesCotizacionCliente] nombre store procedure insertar detalle
                using (SqlCommand cmd = new SqlCommand("pInsertarDocumentoVenta", sql))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@sCo_Tipo_Doc", SqlDbType.Char, (6)).Value = det.sCo_Tipo_Doc;
                    cmd.Parameters.Add("@sNro_Doc", SqlDbType.Char, (20)).Value = det.@sNro_Doc;
                    cmd.Parameters.Add("@sCo_Cli", SqlDbType.Char, (16)).Value = det.@sCo_Cli;
                    cmd.Parameters.Add("@sCo_Ven", SqlDbType.Char, (6)).Value = det.@sCo_Ven;
                    cmd.Parameters.Add("@sCo_Mone", SqlDbType.Char, (6)).Value = det.@sCo_Mone;
                    cmd.Parameters.Add("@sMov_Ban", SqlDbType.Char, (20)).Value = System.Data.SqlTypes.SqlString.Null;
                    cmd.Parameters.Add("@sCo_Cta_Ingr_Egr", SqlDbType.Char, (20)).Value = det.@sCo_Cta_Ingr_Egr;
                    cmd.Parameters.Add("@deTasa", SqlDbType.Decimal).Value = det.@deTasa;
                    cmd.Parameters.Add("@sObserva", SqlDbType.VarChar, (-1)).Value = det.@sObserva;
                    cmd.Parameters.Add("@sdFec_Emis", SqlDbType.SmallDateTime).Value = det.sdFec_Emis;
                    cmd.Parameters.Add("@sdFec_Venc", SqlDbType.SmallDateTime).Value = det.sdFec_Venc;
                    cmd.Parameters.Add("@sdFec_Reg", SqlDbType.SmallDateTime).Value = det.sdFec_Reg;
                    cmd.Parameters.Add("@bAnulado", SqlDbType.Bit).Value = det.bAnulado;
                    cmd.Parameters.Add("@bAut", SqlDbType.Bit).Value = det.@bAut;
                    cmd.Parameters.Add("@bContrib", SqlDbType.Bit).Value = det.@bContrib;
                    cmd.Parameters.Add("@sDoc_Orig", SqlDbType.Char, (6)).Value = det.@sDoc_Orig;
                    cmd.Parameters.Add("@sNro_Orig", SqlDbType.VarChar, (20)).Value = det.@sNro_Orig;
                    cmd.Parameters.Add("@sNro_Che", SqlDbType.VarChar, (20)).Value = System.Data.SqlTypes.SqlString.Null;
                    cmd.Parameters.Add("@deMonto_Imp", SqlDbType.Decimal).Value = det.@deMonto_Imp;
                    cmd.Parameters.Add("@deSaldo", SqlDbType.Decimal).Value = det.@deSaldo;
                    cmd.Parameters.Add("@deTotal_Bruto", SqlDbType.Decimal).Value = det.@deTotal_Bruto;
                    cmd.Parameters.Add("@deMonto_Desc_Glob", SqlDbType.Decimal).Value = det.@deMonto_Desc_Glob;
                    cmd.Parameters.Add("@sPorc_Desc_Glob", SqlDbType.VarChar, (15)).Value = System.Data.SqlTypes.SqlString.Null; ;
                    cmd.Parameters.Add("@sPorc_Reca", SqlDbType.VarChar, (15)).Value = System.Data.SqlTypes.SqlString.Null;
                    cmd.Parameters.Add("@deMonto_Reca", SqlDbType.Decimal).Value = det.@deMonto_Reca;
                    cmd.Parameters.Add("@deTotal_Neto", SqlDbType.Decimal).Value = det.@deTotal_Neto;
                    cmd.Parameters.Add("@deMonto_Imp2", SqlDbType.Decimal).Value = det.@deMonto_Imp2;
                    cmd.Parameters.Add("@deMonto_Imp3", SqlDbType.Decimal).Value = det.@deMonto_Imp2;
                    cmd.Parameters.Add("@sTipo_Imp", SqlDbType.Char, (1)).Value = det.@sTipo_Imp;
                    cmd.Parameters.Add("@iTipo_Origen", SqlDbType.Int).Value = det.@iTipo_Origen;
                    cmd.Parameters.Add("@dePorc_Imp", SqlDbType.Decimal).Value = det.@dePorc_Imp;
                    cmd.Parameters.Add("@dePorc_Imp2", SqlDbType.Decimal).Value = det.@dePorc_Imp2;
                    cmd.Parameters.Add("@dePorc_Imp3", SqlDbType.Decimal).Value = det.@dePorc_Imp3;
                    cmd.Parameters.Add("@sNum_Comprobante", SqlDbType.Char, (14)).Value = System.Data.SqlTypes.SqlString.Null; 
                    cmd.Parameters.Add("@sN_Control", SqlDbType.VarChar, (20)).Value = det.@sN_Control;
                    cmd.Parameters.Add("@sDis_Cen", SqlDbType.VarChar, (-1)).Value = det.@sDis_Cen;
                    cmd.Parameters.Add("@deComis1", SqlDbType.Decimal).Value = det.@deComis1;
                    cmd.Parameters.Add("@deComis2", SqlDbType.Decimal).Value = det.@deComis2;
                    cmd.Parameters.Add("@deComis3", SqlDbType.Decimal).Value = det.@deComis3;
                    cmd.Parameters.Add("@deComis4", SqlDbType.Decimal).Value = det.@deComis4;
                    cmd.Parameters.Add("@deComis5", SqlDbType.Decimal).Value = det.@deComis5;
                    cmd.Parameters.Add("@deComis6", SqlDbType.Decimal).Value = det.@deComis6;
                    cmd.Parameters.Add("@deAdicional", SqlDbType.Decimal).Value = det.@deAdicional;
                    cmd.Parameters.Add("@sSalestax", SqlDbType.Char, (8)).Value = System.Data.SqlTypes.SqlString.Null;
                    cmd.Parameters.Add("@bVen_Ter", SqlDbType.Bit).Value = det.@bVen_Ter;
                    cmd.Parameters.Add("@sImpfis", SqlDbType.VarChar, (20)).Value = det.@sImpfis;
                    cmd.Parameters.Add("@sImpfisfac", SqlDbType.VarChar, (15)).Value = det.@sImpfisfac;
                    cmd.Parameters.Add("@sImp_nro_z", SqlDbType.Char, (15)).Value = det.@sImp_nro_z;
                    cmd.Parameters.Add("@deOtros1", SqlDbType.Decimal).Value = det.@deOtros1;
                    cmd.Parameters.Add("@deOtros2", SqlDbType.Decimal).Value = det.@deOtros2;
                    cmd.Parameters.Add("@deOtros3", SqlDbType.Decimal).Value = det.@deOtros3;
                    cmd.Parameters.Add("@sCampo1", SqlDbType.VarChar, (60)).Value = det.sCampo1;
                    cmd.Parameters.Add("@sCampo2", SqlDbType.VarChar, (60)).Value = det.sCampo2;
                    cmd.Parameters.Add("@sCampo3", SqlDbType.VarChar, (60)).Value = det.sCampo3;
                    cmd.Parameters.Add("@sCampo4", SqlDbType.VarChar, (60)).Value = det.sCampo4;
                    cmd.Parameters.Add("@sCampo5", SqlDbType.VarChar, (60)).Value = det.sCampo5;
                    cmd.Parameters.Add("@sCampo6", SqlDbType.VarChar, (60)).Value = det.sCampo6;
                    cmd.Parameters.Add("@sCampo7", SqlDbType.VarChar, (60)).Value = det.sCampo7;
                    cmd.Parameters.Add("@sCampo8", SqlDbType.VarChar, (60)).Value = det.sCampo8;
                    cmd.Parameters.Add("@sREVISADO", SqlDbType.Char, (1)).Value = System.Data.SqlTypes.SqlDateTime.Null;
                    cmd.Parameters.Add("@sTRASNFE", SqlDbType.Char, (1)).Value = System.Data.SqlTypes.SqlDateTime.Null;
                    cmd.Parameters.Add("@sCo_Sucu_In", SqlDbType.Char, (6)).Value =
                        (sub == "" ? System.Data.SqlTypes.SqlString.Null : sub); ;
                    cmd.Parameters.Add("@sCo_Us_In", SqlDbType.Char, (6)).Value = det.sCo_Us_In;
                    cmd.Parameters.Add("@sMaquina", SqlDbType.VarChar, (60)).Value = det.sMaquina;

                    cmd.Transaction = transaction;
                    cmd.ExecuteNonQuery();

                    respond.Message = "OK";
                    respond.Status = true;

                    return respond;
                }
            }
            catch (Exception ex)
            {
                respond.Message = "Error al insertar Documentos de Ventas";
                respond.Status = false;
                respond.errorMessage = ex.Message;

                return respond;
            }
        }

        public static E_Resp consultarIva(int db, string ffind)
        {
            E_Resp respond = new E_Resp();

            try
            {
                string conectionstring = ConectionString(db);

                using (SqlConnection sql = new SqlConnection(conectionstring))
                {
                    DataTable dt = new DataTable();
                    string query = "SELECT TOP(1) tipo_imp, porc_tasa FROM saImpuestoSobreVentaReng WHERE(tipo_imp = '" + ffind + "') ORDER BY fecha DESC";
                    //string query = "select demoa.dbo.TasaImpuestoSobreVentaAUnaFecha("+ffind+",GETDATE(),1)";

                    
                    SqlCommand cmd = new SqlCommand(query, sql);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    sql.Open();
                    DataSet ds = new DataSet();
                    da.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {
                        respond.Message = "OK";
                        respond.Status = true;
                        respond.Obj = dt;
                    }
                    else
                    {
                        respond.Message = "FAIL";
                        respond.Status = false;
                        respond.Obj = 0;
                    }
                    sql.Close();
                    return respond;
                    
                }
            }
            catch (Exception ex)
            {
                respond.Message = "Error al consultar iva";
                respond.Status = false;
                respond.errorMessage = ex.Message;

                return respond;
            }
        }

        public static E_Resp consultarQuery(string query, int db)
        {
            E_Resp respond = new E_Resp();

            try
            {
                string conectionstring = ConectionString(db);

                using (SqlConnection sql = new SqlConnection(conectionstring))
                {
                    DataTable dt = new DataTable();
                    SqlCommand cmd = new SqlCommand(query, sql);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    sql.Open();
                    DataSet ds = new DataSet();
                    da.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {
                        respond.Message = "OK";
                        respond.Status = true;
                        respond.Obj = dt;
                    }
                    else
                    {
                        respond.Message = "FAIL";
                        respond.Status = false;
                        respond.Obj = 0;
                    }
                    sql.Close();
                    return respond;

                }
            }
            catch (Exception ex)
            {
                respond.Message = "No se encontró el valor buscado.";
                respond.Status = false;
                respond.errorMessage = ex.Message;

                return respond;
            }
        }



    }
}
