﻿using Data_Up;
using Entity_Up;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Business_Up
{
    public class DataConection
    {

        public static E_Resp TestConection(string server, string database, 
                                         string user, string password,
                                         string master)
        {
            string cs = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};",
                            server, database, user, password);
            string cm = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};",
                            server, master, user, password);

            E_Resp resp = new E_Resp();
            resp = Data_Up.db.TestConecion(cs);

            //if (resp.Status)
            //{
            //    resp = Data_Up.db.TestConecion(cm);
            //}
            

            return resp;
        }

        public static E_Resp consultar(string sp)
        {
            E_Resp resp = new E_Resp();
            resp = Data_Up.db.consultar(sp, 1);

            return resp;
        }

        public static E_Resp Exist(string sp, int db, string parameter, string strfind)
        {
            E_Resp resp = new E_Resp();
            resp = Data_Up.db.Exist(sp, db, parameter, strfind);

            return resp;
        }

        public static E_Resp NextConsecutive(string sp, int db, List<SqlParameter> parameters)
        {
            E_Resp resp = new E_Resp();
            resp = Data_Up.db.NextConsecutive(sp, db, parameters);

            return resp;
        }

        public static E_Resp ActualizarTabla(string sp, int db, List<SqlParameter> parameters)
        {
            E_Resp resp = new E_Resp();
            resp = Data_Up.db.ActualizarTabla(sp, db, parameters);

            return resp;
        }

        public static E_Resp InsertRecord(string sp, int db, List<SqlParameter> parameters)
        {
            E_Resp resp = new E_Resp();
            resp = Data_Up.db.InsertRecord(sp, db, parameters);

            return resp;
        }

        public static E_Resp DeleteRecord(string sp, int db, List<SqlParameter> parameters)
        {
            E_Resp resp = new E_Resp();
            resp = Data_Up.db.InsertRecord(sp, db, parameters);

            return resp;
        }

        public static E_Resp ConsultaIBetel(string sp, int db, List<SqlParameter> parameters = null)
        {
            E_Resp resp = new E_Resp();
            resp = Data_Up.db.ConsultaIBetel(sp, db, parameters);

            return resp;
        }

        public static E_Resp ExistIVA(string strfind)
        {
            E_Resp resp = new E_Resp();
            resp = Data_Up.db.consultarIva(1, strfind);

            return resp;
        }

        public static E_Resp ConsultarQuery(string query, int db)
        {
            E_Resp resp = new E_Resp();
            resp = Data_Up.db.consultarQuery(query, 1);

            return resp;
        }

        public static E_Resp InsertCotizacionHeadAll(List<E_CotizacionHead> heads, string subsidiary, List<E_CotizacionDetail> details, bool subs, List<E_DocumentoHead> documents)
        {
            E_Resp resp = new E_Resp();
            resp = Data_Up.db.InsertCotizacionHeadAll(heads, subsidiary, details, subs, documents );

            return resp;
        }


        public static E_XmlFields ReadXml()
        {
            E_XmlFields field = Data_Up.db.ReadXml();
            return field;
        }


    }
}
