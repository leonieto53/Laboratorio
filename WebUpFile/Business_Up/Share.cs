﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Business_Up
{
    public class Share
    {

        public static bool CheckForm<T>() where T : Form, new()
        {
            Form Frm = Application.OpenForms.Cast<Form>().FirstOrDefault(x => x is T);
            if (Frm != null)
            {
                //if form is minimized, put it normal
                if (Frm.WindowState == FormWindowState.Minimized)
                {
                    Frm.WindowState = FormWindowState.Normal;
                }
                // foreground
                Frm.BringToFront();
                return true;
            }
            return false;
            //var frm = new T();
            //Frm = new typeof(T);
            //frm.MdiParent = frmMain;
            //frm. .Userloggedin = userloggedin;
            //Frm.Show();
        }
    }
}
